var namespace_structures_1_1_utilities_1_1_math_functions =
[
    [ "CommonMathFunctions", "class_structures_1_1_utilities_1_1_math_functions_1_1_common_math_functions.html", "class_structures_1_1_utilities_1_1_math_functions_1_1_common_math_functions" ],
    [ "EnvelopeType", "namespace_structures_1_1_utilities_1_1_math_functions.html#ac6e75108cf79deec9359f097f21e9d43", [
      [ "Max", "namespace_structures_1_1_utilities_1_1_math_functions.html#ac6e75108cf79deec9359f097f21e9d43a6a061313d22e51e0f25b7cd4dc065233", null ],
      [ "Min", "namespace_structures_1_1_utilities_1_1_math_functions.html#ac6e75108cf79deec9359f097f21e9d43a78d811e98514cd165dda532286610fd2", null ],
      [ "MaxAbs", "namespace_structures_1_1_utilities_1_1_math_functions.html#ac6e75108cf79deec9359f097f21e9d43a70daa745cdfcc8f37de05539bc55d60d", null ]
    ] ],
    [ "OutsideBoundsTreatment", "namespace_structures_1_1_utilities_1_1_math_functions.html#a6a71445ef1d3f5c8c9f044df9c2a1875", [
      [ "Zero", "namespace_structures_1_1_utilities_1_1_math_functions.html#a6a71445ef1d3f5c8c9f044df9c2a1875ad7ed4ee1df437474d005188535f74875", null ],
      [ "FirstOrLast", "namespace_structures_1_1_utilities_1_1_math_functions.html#a6a71445ef1d3f5c8c9f044df9c2a1875a9a30e5ff235eaa2a2425e0e8849ee601", null ],
      [ "Extrapolate", "namespace_structures_1_1_utilities_1_1_math_functions.html#a6a71445ef1d3f5c8c9f044df9c2a1875a16237c1d30a0f1f53e62983156cfd199", null ]
    ] ]
];