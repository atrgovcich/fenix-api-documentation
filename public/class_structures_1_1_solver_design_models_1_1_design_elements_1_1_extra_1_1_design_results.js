var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results =
[
    [ "DesignResults", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#aca7a7e51fedf08a2592201c33152180d", null ],
    [ "AddDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#ae03546ab455fd9483dea9f70deceeb95", null ],
    [ "ClearDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a45cda000d0634beff0fcdf8952613443", null ],
    [ "DCRforAction", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a55f160f4a97ca74fa0b8da0894fbb43f", null ],
    [ "ActionAtMaximumDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a1c5fe794c4bbaf1c6b156c80ce9ce8b6", null ],
    [ "ListOfDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a8013654d061403a7f4139e814f10bc3d", null ],
    [ "LoadCombo", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a51a8be08ee85cb2f5178290e103ba009", null ],
    [ "MaximumAbsoluteDemand", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#a5ee61760657be006d4e1ea0477d99277", null ],
    [ "MaximumDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html#ab868390f2f7b912ad8dce6ae75b81935", null ]
];