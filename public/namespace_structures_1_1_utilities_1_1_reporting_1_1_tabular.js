var namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular =
[
    [ "ColumnDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_column_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_column_display_settings" ],
    [ "DataTableExtensions", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_data_table_extensions.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_data_table_extensions" ],
    [ "NumericDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_numeric_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_numeric_display_settings" ],
    [ "Row", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_row.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_row" ],
    [ "SortOrder", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order" ],
    [ "StringDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_string_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_string_display_settings" ],
    [ "Table", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table" ],
    [ "Alignment", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a151547c475ed7d88c3612e5650e49e92", [
      [ "Left", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a151547c475ed7d88c3612e5650e49e92a945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Center", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a151547c475ed7d88c3612e5650e49e92a4f1f6016fc9f3f2353c0cc7c67b292bd", null ],
      [ "Right", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a151547c475ed7d88c3612e5650e49e92a92b09c7c48c520c3c55e497875da437c", null ]
    ] ],
    [ "ColumnDataType", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a6084e6f115862ef84a10ddd7ab51b253", [
      [ "Text", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a6084e6f115862ef84a10ddd7ab51b253a9dffbf69ffba8bc38bc4e01abf4b1675", null ],
      [ "Number", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#a6084e6f115862ef84a10ddd7ab51b253ab2ee912b91d69b435159c7c3f6df7f5f", null ]
    ] ],
    [ "StringCase", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#aa0552d4a86c4003c9679aeb095e7ed6c", [
      [ "Unchanged", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#aa0552d4a86c4003c9679aeb095e7ed6ca5ff3c6978f87d96febfdc8ed3899a97e", null ],
      [ "AllCaps", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#aa0552d4a86c4003c9679aeb095e7ed6ca6b4fb9b0044222c4d5ff2118627312db", null ],
      [ "AllLowercase", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html#aa0552d4a86c4003c9679aeb095e7ed6ca583a66755bfa492522a549f676569664", null ]
    ] ]
];