var namespace_structures_1_1_utilities_1_1_svg =
[
    [ "DrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_drawing_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_drawing_properties" ],
    [ "SvgDrawUtility", "class_structures_1_1_utilities_1_1_svg_1_1_svg_draw_utility.html", "class_structures_1_1_utilities_1_1_svg_1_1_svg_draw_utility" ],
    [ "SVGProperties", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties" ],
    [ "TooltipText", "class_structures_1_1_utilities_1_1_svg_1_1_tooltip_text.html", "class_structures_1_1_utilities_1_1_svg_1_1_tooltip_text" ],
    [ "TriangleDrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_triangle_drawing_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_triangle_drawing_properties" ],
    [ "LegendLocation", "namespace_structures_1_1_utilities_1_1_svg.html#a5d2376b708d1ab8b108f2666869945a8", [
      [ "Top", "namespace_structures_1_1_utilities_1_1_svg.html#a5d2376b708d1ab8b108f2666869945a8aa4ffdcf0dc1f31b9acaf295d75b51d00", null ],
      [ "Bottom", "namespace_structures_1_1_utilities_1_1_svg.html#a5d2376b708d1ab8b108f2666869945a8a2ad9d63b69c4a10a5cc9cad923133bc4", null ],
      [ "Left", "namespace_structures_1_1_utilities_1_1_svg.html#a5d2376b708d1ab8b108f2666869945a8a945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Right", "namespace_structures_1_1_utilities_1_1_svg.html#a5d2376b708d1ab8b108f2666869945a8a92b09c7c48c520c3c55e497875da437c", null ]
    ] ]
];