var class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions =
[
    [ "Edges", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a1d5d6338169614f53ee36bce6370efd5", null ],
    [ "GetPlane", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#ae391c40048e9c038f2fb59d45cd02bd0", null ],
    [ "GetPlaneFromVertexList", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a917e123eff2260e9280e83a28f3796f5", null ],
    [ "IntersectsWithOtherPolygon", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#aea57fbfbec6a379a95396aba3230ac93", null ],
    [ "IntersectsWithPolygon", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a70325b9c9100327ac610e56f9d19c513", null ],
    [ "LiesInSamePlaneAs", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a23cd80390ea6d8160136e4ab6cb18d69", null ],
    [ "Normal", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a65c060ad8f7105502fe855be006f4589", null ],
    [ "PointRotationMatrixGlobalToLocal", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#aead4606c3db4b35574a8e7ccdedf1fa2", null ],
    [ "PolygonLocalToGlobalRotationMatrix", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#ad81e8bfc65f0e87e153bafe6d091a73d", null ],
    [ "ToPolygon2D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a579d06af41d8a22873d2e742558545bf", null ],
    [ "Translate", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a7c1e7993d88b3ca409d6fb29d50b1f49", null ],
    [ "ORTHO_TOLERANCE", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html#a6b16f8f0c06963bb83dfadd9d74e3f9e", null ]
];