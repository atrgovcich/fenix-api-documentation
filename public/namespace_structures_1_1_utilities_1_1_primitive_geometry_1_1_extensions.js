var namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions =
[
    [ "LineSegment2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment2_d_extensions" ],
    [ "LineSegment3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions" ],
    [ "PlaneExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions" ],
    [ "Point2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point2_d_extensions" ],
    [ "Point3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point3_d_extensions" ],
    [ "Polygon2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions" ],
    [ "Polygon3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions" ],
    [ "Polyline2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polyline2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polyline2_d_extensions" ],
    [ "Vector2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector2_d_extensions" ],
    [ "Vector3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector3_d_extensions" ],
    [ "Side", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html#a4e1b42fe28c7830adf8c973fefa5b46a", [
      [ "Right", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html#a4e1b42fe28c7830adf8c973fefa5b46aa92b09c7c48c520c3c55e497875da437c", null ],
      [ "Left", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html#a4e1b42fe28c7830adf8c973fefa5b46aa945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Colinear", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html#a4e1b42fe28c7830adf8c973fefa5b46aaa3b4c37725a0edfb81d4c14c3946079b", null ]
    ] ]
];