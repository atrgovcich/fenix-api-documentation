var hierarchy =
[
    [ "Structures.Utilities.Loads.AccidentalEccentricySettings", "class_structures_1_1_utilities_1_1_loads_1_1_accidental_eccentricy_settings.html", null ],
    [ "Structures.Solver3D.Nodes.AnalysisNode", "class_structures_1_1_solver3_d_1_1_nodes_1_1_analysis_node.html", null ],
    [ "Structures.Solver3D.General.AnalysisResult< T >", "class_structures_1_1_solver3_d_1_1_general_1_1_analysis_result.html", null ],
    [ "Structures.MathNetCustom.Spatial.Units.AngleUnit", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle_unit.html", null ],
    [ "Structures.Solver3D.Elements.Point.AreaSpringProperties", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_area_spring_properties.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.ArrayExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_array_extensions.html", null ],
    [ "Structures.Utilities.HttpRequests.ASCE_7_Hazard_Tool", "class_structures_1_1_utilities_1_1_http_requests_1_1_a_s_c_e__7___hazard___tool.html", null ],
    [ "Structures.Solver3D.General.AutoElfCalculationVariables", "class_structures_1_1_solver3_d_1_1_general_1_1_auto_elf_calculation_variables.html", null ],
    [ "Structures.Utilities.Loads.AutoLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_auto_lateral_force.html", [
      [ "Structures.Utilities.Loads.ASCE_EquivlaentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e___equivlaent_lateral_force.html", [
        [ "Structures.Utilities.Loads.ASCE41_17_EquivalentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e41__17___equivalent_lateral_force.html", null ],
        [ "Structures.Utilities.Loads.ASCE7_16_EquivalentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___equivalent_lateral_force.html", null ],
        [ "Structures.Utilities.Loads.ASCE7_16_UserCs", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___user_cs.html", null ]
      ] ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.AreaElements.AutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_auto_mesh_options.html", null ],
    [ "Structures.Solver3D.Loads.BodyForce", "class_structures_1_1_solver3_d_1_1_loads_1_1_body_force.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.BoundingBox", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounding_box.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.BoundsOctree< T >", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounds_octree.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.BoundsOctree< IGeneralElement >", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounds_octree.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.BracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html", null ],
    [ "Structures.Utilities.BuildingCodes.BuildingCode", "class_structures_1_1_utilities_1_1_building_codes_1_1_building_code.html", null ],
    [ "Structures.Utilities.Reporting.Equations.CalculationLog", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_calculation_log.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.CharExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_char_extensions.html", null ],
    [ "Structures.Utilities.Charting.Chart", "class_structures_1_1_utilities_1_1_charting_1_1_chart.html", [
      [ "Structures.Utilities.Charting.Chart2D", "class_structures_1_1_utilities_1_1_charting_1_1_chart2_d.html", null ]
    ] ],
    [ "Structures.Utilities.Geometry.CircleDefinition", "struct_structures_1_1_utilities_1_1_geometry_1_1_circle_definition.html", null ],
    [ "Structures.Materials.Elastic.Wood.CLTLayer", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_c_l_t_layer.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.Extra.ColdFormedResources", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_cold_formed_resources.html", null ],
    [ "Structures.Utilities.Graphics.ColorGradient", "class_structures_1_1_utilities_1_1_graphics_1_1_color_gradient.html", null ],
    [ "Structures.Utilities.Reporting.Tabular.ColumnDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_column_display_settings.html", [
      [ "Structures.Utilities.Reporting.Tabular.NumericDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_numeric_display_settings.html", null ],
      [ "Structures.Utilities.Reporting.Tabular.StringDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_string_display_settings.html", null ]
    ] ],
    [ "Structures.Utilities.MathFunctions.CommonMathFunctions", "class_structures_1_1_utilities_1_1_math_functions_1_1_common_math_functions.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CompositeSteelBeamProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_composite_steel_beam_properties.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ConcreteSectionShearDepth", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectaa3691aad016020bea4a6d149f18d860.html", null ],
    [ "Structures.Materials.Inelastic.Concrete.ConfinementProperties", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confinement_properties.html", [
      [ "Structures.Materials.Inelastic.Concrete.CircularConfinementProperties", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_circular_confinement_properties.html", null ]
    ] ],
    [ "Structures.Materials.General.Constants", "class_structures_1_1_materials_1_1_general_1_1_constants.html", null ],
    [ "Structures.Utilities.Units.Conversion.Constants", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_constants.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.Constraints", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectae45e40362383fba770f534e67206fcf.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.ConstraintSpring", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcdd419d06a3b5df2fefdfedc6f508cea.html", null ],
    [ "Structures.MathNetCustom.Spatial.Internals.ConvexHull", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_internals_1_1_convex_hull.html", null ],
    [ "Structures.Solver3D.CoordinateConversions.CoordinateSystemOperations", "class_structures_1_1_solver3_d_1_1_coordinate_conversions_1_1_coordinate_system_operations.html", null ],
    [ "Structures.Solver3D.Elements.Linear.CrossSection", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_cross_section.html", [
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_design_cross_section.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.DesignColdFormedSteelSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fd4dec1ca8e57adcfe96b7cfadb0a.html", [
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.DesignColdFormedCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect17ee1eee13d1f284a1ef60f4af1be67e.html", [
            [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.DesignColdFormedBackToBackCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect30a7837db523120846bbeed1e59667b6.html", [
              [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.DesignColdFormedBoxedBackToBackCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect28e871ae269a66f14fe91309405f1797.html", null ]
            ] ]
          ] ]
        ] ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignAssembledCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte76ff215972dbccf3d987113e8562856.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCircularCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectd857442166a589c4dee5ef3d70648e41.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCoreBraceBRBSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5213eef38b8d6692354c5c2c68ac2fb0.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCustomConcreteSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9edb935761261f31aebfbc0fd277797b.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCustomHomogeneousSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect6e92485a05232ea2dafe700adfecbcde.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignRectangularCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect33b00aabffcc22c4ef9eff65e48942b1.html", [
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignStructuralCompositeLumberSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8ed84c03eb2bb63e164b3b793960e010.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.Wood.DesignGlulamCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf7cefac20e8da9dfe3b91dbf30897038.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.Wood.DesignSawnLumberCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5eb2e6e83dc58e2a8f1d8ec63e344097.html", null ]
        ] ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignSteelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect1fed346bee88887f93c8487c9adc6176.html", [
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignChannelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcda38547983eb661565302548f4c504f.html", [
            [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignMiscChannelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf6293eb321d242a02b281208cc305d9a.html", null ],
            [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignParallelFlangeChannel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect034839b75bde3fdd88aea3eb06b0df06.html", null ]
          ] ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignRectangularHSSCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2200add24e6878d457d37d1cf03f9383.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignRoundHSSCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect450fafc59b666c16e873189322376d9a.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignSingleAngleCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2456887f1b32d13b065ef4ceb00633a5.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignWideFlangeCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2dd16171a089eff440825a6f6d8f0095.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignWideFlangeTeeCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect7ef1c6bfd95f15d93d5337668a92d547.html", null ]
        ] ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignWoodIJoistSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc358d39d6dde640f7860d59e9489153d.html", null ]
      ] ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.CrossSectionAssemblyItem", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5f174aab34cce1a2149831b743178020.html", null ],
    [ "Structures.SolverDesignModels.Extensions.CurveExtensions", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_curve_extensions.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.CustomSectionGeometry", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9bc863e4dbc454e81746b778ea5d441d.html", null ],
    [ "Structures.Utilities.HttpRequests.Data", "class_structures_1_1_utilities_1_1_http_requests_1_1_data.html", null ],
    [ "Structures.Utilities.Reporting.Tabular.DataTableExtensions", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_data_table_extensions.html", null ],
    [ "Structures.Solver3D.Geometry.DegreeOfFreedomFixity", "class_structures_1_1_solver3_d_1_1_geometry_1_1_degree_of_freedom_fixity.html", null ],
    [ "Structures.Solver3D.Elements.Linear.DegreeOfFreedomRelease", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_degree_of_freedom_release.html", null ],
    [ "MathNet.Numerics.LinearAlgebra.Double.DenseMatrix", null, [
      [ "Structures.MathNetCustom.Spatial.Euclidean.CoordinateSystem", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_coordinate_system.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignAreaElementObjectives", "class_structures_1_1_solver_design_models_1_1_design_area_element_objectives.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_load.html", [
      [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignLinearlyVaryingAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_area_load.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignLinearlyVaryingPercentAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_percent_area_load.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignBendingProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectb52f30af52a56ca03fa466760f9bd2c3.html", null ],
    [ "Structures.SolverDesignModels.DesignCrossSectionObjectives", "class_structures_1_1_solver_design_models_1_1_design_cross_section_objectives.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.DesignElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_element.html", [
      [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignAreaElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_element.html", [
        [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignCLTElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_c_l_t_element.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignOffsetAreaElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_offset_area_element.html", null ]
      ] ],
      [ "Structures.SolverDesignModels.DesignElements.DesignDelayedRestraint", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_restraint.html", [
        [ "Structures.SolverDesignModels.DesignElements.DesignDelayedAreaRestraint", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_area_restraint.html", [
          [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignAreaSpring", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring.html", null ],
          [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignAreaSupport", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_support.html", null ]
        ] ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignLineSupport", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_line_support.html", null ]
      ] ],
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_element.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignOffsetFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_offset_frame_element.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignSteelCompositeFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_steel_composite_frame_element.html", null ]
      ] ],
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_link_element.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignGlobalLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_global_link_element.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignLocalLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_local_link_element.html", null ]
      ] ],
      [ "Structures.SolverDesignModels.DesignElements.PointElements.DesignPointElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_point_element.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.SolidElements.DesignSolidElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_element.html", [
        [ "Structures.SolverDesignModels.DesignElements.SolidElements.DesignHexahedronElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_hexahedron_element.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.SolidElements.DesignPentahedronElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_pentahedron_element.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.SolidElements.DesignSolidExtrusion", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html", null ]
      ] ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.DesignGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_group.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.DesignNodalDisplacement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_displacement.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.DesignNodalForce", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_force.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.Extra.DesignObject", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_object.html", [
      [ "Structures.SolverDesignModels.DesignElements.Extra.SheathingPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_panel.html", [
        [ "Structures.SolverDesignModels.DesignElements.Extra.DiaphragmPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.Extra.ShearWallPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html", null ]
      ] ]
    ] ],
    [ "Structures.SolverDesignModels.Settings.DesignReportSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.DesignRestraintProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_restraint_properties.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.Extra.DesignResults", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.SolidElements.DesignSolidLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_load.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignTorsionalProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8249da24b5b0f4c13ab908604a1a19a2.html", null ],
    [ "Structures.Utilities.SolverSupport.Diaphragm", "class_structures_1_1_utilities_1_1_solver_support_1_1_diaphragm.html", null ],
    [ "Structures.Solver3D.General.DiaphragmDefinition", "class_structures_1_1_solver3_d_1_1_general_1_1_diaphragm_definition.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.Dimensions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fe4eeeb14610cac8744faac81f948.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.DistortionalBucklingProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secta29fa85140e4ad9687aa0caa9bc28374.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.DoubleExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html", null ],
    [ "Structures.Utilities.Svg.DrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_drawing_properties.html", [
      [ "Structures.Utilities.Svg.TriangleDrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_triangle_drawing_properties.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.DriftStack", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_stack.html", null ],
    [ "Structures.Utilities.Geometry.Edge", "class_structures_1_1_utilities_1_1_geometry_1_1_edge.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.Wood.EffectiveCharDepth", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc3e247cfa5c615cce608bbce952fa11b.html", null ],
    [ "Structures.Solver3D.Solver.MatrixSupport.EigenMatrixMath.ElementCondensationReturn", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_eigen_matrix_math_1_1_element_condensation_return.html", null ],
    [ "Structures.Solver3D.Elements.ElementResult", "class_structures_1_1_solver3_d_1_1_elements_1_1_element_result.html", [
      [ "Structures.Solver3D.Elements.Linear.FrameElementEndForce", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_force.html", null ],
      [ "Structures.Solver3D.Elements.Linear.LinkElementEndForce", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element_end_force.html", null ],
      [ "Structures.Solver3D.Elements.Planar.ShellStress", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress.html", null ],
      [ "Structures.Solver3D.Elements.Solid.SolidStress", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress.html", null ],
      [ "Structures.Solver3D.General.SectionCutForce", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_force.html", null ],
      [ "Structures.Solver3D.Nodes.NodalDisplacement", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_displacement.html", null ],
      [ "Structures.Solver3D.Nodes.NodalReaction", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_reaction.html", null ]
    ] ],
    [ "Structures.Utilities.Extensions.DotNetNative.EnumExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_enum_extensions.html", null ],
    [ "Structures.Utilities.Reporting.Equations.EquationDisplayParameters", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_equation_display_parameters.html", null ],
    [ "Structures.SolverDesignModels.Models.EtabsExporter", "class_structures_1_1_solver_design_models_1_1_models_1_1_etabs_exporter.html", null ],
    [ "EventArgs", null, [
      [ "Structures.Utilities.Containers.EventListArgs< T >", "class_structures_1_1_utilities_1_1_containers_1_1_event_list_args.html", null ]
    ] ],
    [ "Exception", null, [
      [ "Structures.Utilities.PrimitiveGeometry.Helpers.SimplePolygonException", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers_1_1_simple_polygon_exception.html", null ],
      [ "Structures.Utilities.TriangleNetHelper.TriangleNetInvalidPolygonException", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_net_invalid_polygon_exception.html", null ]
    ] ],
    [ "Structures.Utilities.Logging.FenixLogger", "class_structures_1_1_utilities_1_1_logging_1_1_fenix_logger.html", null ],
    [ "Structures.Solver3D.Solver.FiniteElementSolver3D", "class_structures_1_1_solver3_d_1_1_solver_1_1_finite_element_solver3_d.html", [
      [ "Structures.Solver3D.Solver.IterativeFiniteElementSolver3D", "class_structures_1_1_solver3_d_1_1_solver_1_1_iterative_finite_element_solver3_d.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.FrameAutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.FrameBracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_bracing_options.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.FrameDisplacementDiagram", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_displacement_diagram.html", null ],
    [ "Structures.Solver3D.Elements.Linear.FrameElementEndRelease", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_release.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.FrameInternalForceOrdinate", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_internal_force_ordinate.html", null ],
    [ "Structures.Utilities.HttpRequests.GeneralQueries", "class_structures_1_1_utilities_1_1_http_requests_1_1_general_queries.html", null ],
    [ "Structures.SolverDesignModels.Models.GenericDesignModel", "class_structures_1_1_solver_design_models_1_1_models_1_1_generic_design_model.html", null ],
    [ "Structures.Utilities.HttpRequests.Geolocation", "class_structures_1_1_utilities_1_1_http_requests_1_1_geolocation.html", null ],
    [ "Structures.Utilities.Geometry.GeometryFunctions", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry_functions.html", null ],
    [ "Structures.Utilities.TriangleNetHelper.GeometryFunctions", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_functions.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.GeometryHelper", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_geometry_helper.html", null ],
    [ "Structures.Utilities.Geospatial.GeospatialMethods", "class_structures_1_1_utilities_1_1_geospatial_1_1_geospatial_methods.html", null ],
    [ "Structures.SolverDesignModels.GlobalModelParameters", "class_structures_1_1_solver_design_models_1_1_global_model_parameters.html", null ],
    [ "Structures.Solver3D.GlobalSolverParameters", "class_structures_1_1_solver3_d_1_1_global_solver_parameters.html", null ],
    [ "Structures.Materials.Elastic.Wood.GradingAgencyTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_agency_type_functions.html", null ],
    [ "Structures.Materials.Elastic.Wood.GradingMethodTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_method_type_functions.html", null ],
    [ "Structures.SolverDesignModels.Models.GridLine", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_line.html", null ],
    [ "Structures.SolverDesignModels.Models.GridSystem", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_system.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.HoleProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte9ce7ed942f684b33429e3ca566476ef.html", null ],
    [ "Structures.Utilities.Reporting.Html", "class_structures_1_1_utilities_1_1_reporting_1_1_html.html", null ],
    [ "Structures.Solver3D.Elements.Planar.IBendingFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_bending_formulation.html", [
      [ "Structures.Solver3D.Elements.Planar.Quadrilateral.QuadBendingFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_bending_formulation.html", [
        [ "Structures.Solver3D.Elements.Planar.Quadrilateral.DkmqFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkmq_formulation.html", null ],
        [ "Structures.Solver3D.Elements.Planar.Quadrilateral.DkqFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkq_formulation.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Planar.Triangular.TriangularBendingFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_bending_formulation.html", [
        [ "Structures.Solver3D.Elements.Planar.Triangular.DkmtFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkmt_formulation.html", null ],
        [ "Structures.Solver3D.Elements.Planar.Triangular.DktFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkt_formulation.html", null ]
      ] ]
    ] ],
    [ "Structures.Utilities.Graphics.IColorizable", "interface_structures_1_1_utilities_1_1_graphics_1_1_i_colorizable.html", [
      [ "Structures.Materials.Elastic.ElasticMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_elastic_material.html", [
        [ "Structures.Materials.Elastic.IsotropicMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_isotropic_material.html", [
          [ "Structures.SolverDesignModels.Materials.DesignIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_isotropic_material.html", [
            [ "Structures.SolverDesignModels.Materials.AluminumMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_aluminum_material.html", null ],
            [ "Structures.SolverDesignModels.Materials.GeneralIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_isotropic_material.html", null ],
            [ "Structures.SolverDesignModels.Materials.MasonryMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_masonry_material.html", null ]
          ] ]
        ] ],
        [ "Structures.Materials.Elastic.OrthotropicMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_orthotropic_material.html", [
          [ "Structures.Materials.Elastic.Wood.WoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_material.html", [
            [ "Structures.Materials.Elastic.Wood.CrossLaminatedTimberProduct", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_cross_laminated_timber_product.html", null ],
            [ "Structures.Materials.Elastic.Wood.GluedLaminatedMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_glued_laminated_material.html", null ],
            [ "Structures.Materials.Elastic.Wood.LaminatedStrandLumberMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_material.html", null ],
            [ "Structures.Materials.Elastic.Wood.SolidWoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_solid_wood_material.html", null ]
          ] ],
          [ "Structures.SolverDesignModels.Materials.DesignOrthotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_orthotropic_material.html", [
            [ "Structures.SolverDesignModels.Materials.GeneralOrthotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_orthotropic_material.html", null ]
          ] ]
        ] ],
        [ "Structures.Materials.Inelastic.InelasticMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_inelastic_material.html", [
          [ "Structures.Materials.Inelastic.Concrete.InelasticConcreteMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_inelastic_concrete_material.html", [
            [ "Structures.Materials.Inelastic.Concrete.ConfinedConcreteMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confined_concrete_material.html", [
              [ "Structures.Materials.Inelastic.Concrete.ManderConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_confined_concrete.html", null ],
              [ "Structures.Materials.Inelastic.Concrete.PiecewiseLinearConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_piecewise_linear_confined_concrete.html", null ],
              [ "Structures.Materials.Inelastic.Concrete.RestrepoConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_restrepo_confined_concrete.html", null ]
            ] ],
            [ "Structures.Materials.Inelastic.Concrete.HsuHighStrengthConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_hsu_high_strength_concrete.html", null ],
            [ "Structures.Materials.Inelastic.Concrete.ManderConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_concrete.html", [
              [ "Structures.SolverDesignModels.Materials.UnconfinedConcreteMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_unconfined_concrete_material.html", null ]
            ] ]
          ] ],
          [ "Structures.Materials.Inelastic.Steel.InelasticSteelMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_inelastic_steel_material.html", [
            [ "Structures.Materials.Inelastic.Steel.KentSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_kent_steel.html", null ],
            [ "Structures.Materials.Inelastic.Steel.ManderSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_mander_steel.html", [
              [ "Structures.SolverDesignModels.Materials.SteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_steel_material.html", null ]
            ] ],
            [ "Structures.Materials.Inelastic.Steel.MenegottoPintoSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_menegotto_pinto_steel.html", null ],
            [ "Structures.Materials.Inelastic.Steel.ModifiedRambergOsgoodSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_modified_ramberg_osgood_steel.html", [
              [ "Structures.SolverDesignModels.Materials.PrestressingSteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_prestressing_steel_material.html", null ]
            ] ]
          ] ]
        ] ]
      ] ],
      [ "Structures.SolverDesignModels.DesignElements.ElementGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.DesignCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_design_cross_section.html", null ]
    ] ],
    [ "IComparable", null, [
      [ "Structures.MathNetCustom.Spatial.Units.Angle", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.IDesignFrameElement", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_design_frame_element.html", [
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_element.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.IOffsetFrameElement", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_offset_frame_element.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignOffsetFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_offset_frame_element.html", null ]
      ] ]
    ] ],
    [ "Structures.SolverDesignModels.Materials.IDesignMaterial", "interface_structures_1_1_solver_design_models_1_1_materials_1_1_i_design_material.html", [
      [ "Structures.SolverDesignModels.Materials.DesignIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_isotropic_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.DesignOrthotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_orthotropic_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.PrestressingSteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_prestressing_steel_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.SteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_steel_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.UnconfinedConcreteMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_unconfined_concrete_material.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.IDesignSpringProperties", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_i_design_spring_properties.html", [
      [ "Structures.SolverDesignModels.DesignElements.AreaElements.DesignAreaSpringProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring_properties.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.PointElements.DesignSpringProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_spring_properties.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "Structures.Solver3D.Solver.MatrixSupport.IGenericMatrix", "interface_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_i_generic_matrix.html", [
        [ "Structures.Solver3D.Solver.MatrixSupport.SparseGenericMatrix", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_sparse_generic_matrix.html", null ]
      ] ],
      [ "Structures.Solver3D.Solver.MatrixSupport.SparseGenericMatrix", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_sparse_generic_matrix.html", null ]
    ] ],
    [ "Structures.Materials.Elastic.IElasticMaterial", "interface_structures_1_1_materials_1_1_elastic_1_1_i_elastic_material.html", [
      [ "Structures.Materials.Elastic.ElasticMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_elastic_material.html", null ]
    ] ],
    [ "IEnumerable", null, [
      [ "Structures.MathNetCustom.Spatial.Euclidean.PolyLine3D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_poly_line3_d.html", null ]
    ] ],
    [ "Structures.Utilities.DotNetNative.Extensions.IEnumerableExtensions", "class_structures_1_1_utilities_1_1_dot_net_native_1_1_extensions_1_1_i_enumerable_extensions.html", null ],
    [ "IEquatable", null, [
      [ "Structures.MathNetCustom.Spatial.Euclidean.Circle3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_circle3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.CoordinateSystem", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_coordinate_system.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.EulerAngles", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_euler_angles.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Line3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.LineSegment3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line_segment3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Plane", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_plane.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Point3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.PolyLine3D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_poly_line3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Quaternion", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_quaternion.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Ray3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.UnitVector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.UnitVector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Vector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Vector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Units.Angle", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle.html", null ],
      [ "Structures.SolverDesignModels.DesignElements.Extra.StudShape", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html", null ],
      [ "Structures.SolverDesignModels.Materials.DesignIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_isotropic_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.PrestressingSteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_prestressing_steel_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.SteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_steel_material.html", null ],
      [ "Structures.SolverDesignModels.Materials.UnconfinedConcreteMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_unconfined_concrete_material.html", null ],
      [ "Structures.Utilities.Generic.Enumeration", "class_structures_1_1_utilities_1_1_generic_1_1_enumeration.html", [
        [ "Structures.SolverDesignModels.DesignElements.Extra.Mils", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.Extra.Screw", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_screw.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.Extra.SheathingMaterial", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_material.html", null ],
        [ "Structures.SolverDesignModels.DesignElements.Extra.SheathingThickness", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_thickness.html", null ],
        [ "Structures.Utilities.Loads.LoadDurationType", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html", null ],
        [ "Structures.Utilities.Reporting.Tabular.SortOrder", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order.html", null ],
        [ "Structures.Utilities.Units.Gauge", "class_structures_1_1_utilities_1_1_units_1_1_gauge.html", null ],
        [ "Structures.Utilities.Units.Unit", "class_structures_1_1_utilities_1_1_units_1_1_unit.html", [
          [ "Structures.Utilities.Units.AccelerationUnit", "class_structures_1_1_utilities_1_1_units_1_1_acceleration_unit.html", null ],
          [ "Structures.Utilities.Units.AreaUnit", "class_structures_1_1_utilities_1_1_units_1_1_area_unit.html", null ],
          [ "Structures.Utilities.Units.DensityUnit", "class_structures_1_1_utilities_1_1_units_1_1_density_unit.html", null ],
          [ "Structures.Utilities.Units.FlexuralStiffnessUnit", "class_structures_1_1_utilities_1_1_units_1_1_flexural_stiffness_unit.html", null ],
          [ "Structures.Utilities.Units.ForcePerLengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_force_per_length_unit.html", null ],
          [ "Structures.Utilities.Units.ForceUnit", "class_structures_1_1_utilities_1_1_units_1_1_force_unit.html", null ],
          [ "Structures.Utilities.Units.LengthCubedUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_cubed_unit.html", null ],
          [ "Structures.Utilities.Units.LengthToThe4thUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the4th_unit.html", null ],
          [ "Structures.Utilities.Units.LengthToThe6thUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the6th_unit.html", null ],
          [ "Structures.Utilities.Units.LengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_unit.html", null ],
          [ "Structures.Utilities.Units.MomentPerLengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_moment_per_length_unit.html", null ],
          [ "Structures.Utilities.Units.MomentUnit", "class_structures_1_1_utilities_1_1_units_1_1_moment_unit.html", null ],
          [ "Structures.Utilities.Units.StressUnit", "class_structures_1_1_utilities_1_1_units_1_1_stress_unit.html", null ],
          [ "Structures.Utilities.Units.TimeUnit", "class_structures_1_1_utilities_1_1_units_1_1_time_unit.html", null ],
          [ "Structures.Utilities.Units.UndefinedUnit", "class_structures_1_1_utilities_1_1_units_1_1_undefined_unit.html", null ],
          [ "Structures.Utilities.Units.UnitlessUnit", "class_structures_1_1_utilities_1_1_units_1_1_unitless_unit.html", null ]
        ] ]
      ] ],
      [ "Structures.Utilities.TriangleNetHelper.Geometry.PreEdge", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_edge.html", null ],
      [ "Structures.Utilities.Units.Conversion.FLT", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_f_l_t.html", null ],
      [ "Structures.Utilities.Units.Conversion.Result", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_result.html", [
        [ "Structures.Utilities.Units.Conversion.Acceleration", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_acceleration.html", null ],
        [ "Structures.Utilities.Units.Conversion.Area", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_area.html", null ],
        [ "Structures.Utilities.Units.Conversion.Density", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_density.html", null ],
        [ "Structures.Utilities.Units.Conversion.FlexuralStiffness", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_flexural_stiffness.html", null ],
        [ "Structures.Utilities.Units.Conversion.Force", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force.html", null ],
        [ "Structures.Utilities.Units.Conversion.ForcePerLength", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force_per_length.html", null ],
        [ "Structures.Utilities.Units.Conversion.Length", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length.html", null ],
        [ "Structures.Utilities.Units.Conversion.LengthCubed", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_cubed.html", null ],
        [ "Structures.Utilities.Units.Conversion.LengthToThe4th", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the4th.html", null ],
        [ "Structures.Utilities.Units.Conversion.LengthToThe6th", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the6th.html", null ],
        [ "Structures.Utilities.Units.Conversion.Moment", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_moment.html", null ],
        [ "Structures.Utilities.Units.Conversion.Stress", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_stress.html", null ],
        [ "Structures.Utilities.Units.Conversion.Time", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_time.html", null ],
        [ "Structures.Utilities.Units.Conversion.Undefined", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_undefined.html", null ],
        [ "Structures.Utilities.Units.Conversion.Unitless", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_unitless.html", null ]
      ] ]
    ] ],
    [ "IFormattable", null, [
      [ "Structures.MathNetCustom.Spatial.Euclidean.Point3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Quaternion", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_quaternion.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Ray3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.UnitVector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Vector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Units.Angle", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.Connections.IFrameConnection", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connections_1_1_i_frame_connection.html", [
      [ "Structures.SolverDesignModels.DesignElements.LinearElements.Connections.ColdFormedConnections.IColdFormedConnection", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connec967689511b25c885883bc36b25c9d573.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.Connections.ColdFormedConnections.FlangeToFlangeConnection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connection622a412699a42db29e6f6e210a75ae94.html", null ]
      ] ]
    ] ],
    [ "Structures.Solver3D.Elements.IGeneralElement", "interface_structures_1_1_solver3_d_1_1_elements_1_1_i_general_element.html", [
      [ "Structures.Solver3D.Elements.Linear.FrameElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element.html", [
        [ "Structures.Solver3D.Elements.Linear.TimoshenkoBeamElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_timoshenko_beam_element.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Linear.LinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element.html", [
        [ "Structures.Solver3D.Elements.Linear.GlobalLinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_global_link_element.html", null ],
        [ "Structures.Solver3D.Elements.Linear.LocalLinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_local_link_element.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Planar.ShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_element.html", [
        [ "Structures.Solver3D.Elements.Planar.Quadrilateral.QuadShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_shell_element.html", null ],
        [ "Structures.Solver3D.Elements.Planar.Triangular.TriangularShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_shell_element.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Point.OneNode3DSpringElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_one_node3_d_spring_element.html", null ],
      [ "Structures.Solver3D.Elements.Solid.SolidElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_element.html", [
        [ "Structures.Solver3D.Elements.Solid.HexahedronElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedron_element.html", null ],
        [ "Structures.Solver3D.Elements.Solid.PentahedronElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedron_element.html", null ]
      ] ]
    ] ],
    [ "Structures.Utilities.Geometry.IGenericGeometry", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_geometry.html", [
      [ "Structures.Utilities.Geometry.GenericShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_shape.html", [
        [ "Structures.Utilities.Geometry.GenericClosedShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape.html", [
          [ "Structures.Utilities.Geometry.GenericCircle", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle.html", null ],
          [ "Structures.Utilities.Geometry.GenericPolygon", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polygon.html", null ]
        ] ],
        [ "Structures.Utilities.Geometry.GenericPoint", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_point.html", null ],
        [ "Structures.Utilities.Geometry.GenericPolyline", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polyline.html", null ]
      ] ],
      [ "Structures.Utilities.Geometry.IGenericClosedShape", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_closed_shape.html", [
        [ "Structures.Utilities.Geometry.GenericClosedShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape.html", null ]
      ] ]
    ] ],
    [ "Structures.Materials.IGenericMaterial", "interface_structures_1_1_materials_1_1_i_generic_material.html", [
      [ "Structures.Materials.Elastic.ElasticMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_elastic_material.html", null ]
    ] ],
    [ "Structures.Utilities.Threading.ProcessorCores.IHardwareCore", "interface_structures_1_1_utilities_1_1_threading_1_1_processor_cores_1_1_i_hardware_core.html", null ],
    [ "IList", null, [
      [ "Structures.Utilities.Containers.ObservableList< T >", "class_structures_1_1_utilities_1_1_containers_1_1_observable_list.html", null ]
    ] ],
    [ "Structures.Utilities.Extensions.DotNetNative.ImageExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_image_extensions.html", null ],
    [ "Structures.Solver3D.Elements.Planar.IMembraneFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_membrane_formulation.html", [
      [ "Structures.Solver3D.Elements.Planar.Quadrilateral.QuadMembraneFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_membrane_formulation.html", [
        [ "Structures.Solver3D.Elements.Planar.Quadrilateral.QbdFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_qbd_formulation.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Planar.Triangular.TriangularMembraneFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_membrane_formulation.html", [
        [ "Structures.Solver3D.Elements.Planar.Triangular.OptFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_opt_formulation.html", null ]
      ] ]
    ] ],
    [ "Structures.Solver3D.Loads.ImposedNodalDisplacement", "class_structures_1_1_solver3_d_1_1_loads_1_1_imposed_nodal_displacement.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.InclinedRotations", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.IntegerExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_integer_extensions.html", null ],
    [ "IObservable", null, [
      [ "Structures.Utilities.Observable.ObservableString", "class_structures_1_1_utilities_1_1_observable_1_1_observable_string.html", null ]
    ] ],
    [ "Structures.Utilities.TriangleNetHelper.Geometry.IShape", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html", [
      [ "Structures.Utilities.TriangleNetHelper.Geometry.PreEdge", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_edge.html", null ],
      [ "Structures.Utilities.TriangleNetHelper.Geometry.PrePolygon", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polygon.html", null ],
      [ "Structures.Utilities.TriangleNetHelper.Geometry.PrePolyline", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polyline.html", null ],
      [ "Structures.Utilities.TriangleNetHelper.Geometry.PreVertex", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_vertex.html", null ]
    ] ],
    [ "Structures.Solver3D.Elements.Solid.ISolidFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_i_solid_formulation.html", [
      [ "Structures.Solver3D.Elements.Solid.HexahedralFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedral_formulation.html", [
        [ "Structures.Solver3D.Elements.Solid.IsoP20NodeHexahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p20_node_hexahedron.html", null ],
        [ "Structures.Solver3D.Elements.Solid.IsoP8NodeHexahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p8_node_hexahedron.html", null ]
      ] ],
      [ "Structures.Solver3D.Elements.Solid.PentahedralFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedral_formulation.html", [
        [ "Structures.Solver3D.Elements.Solid.IsoP15NodePentahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p15_node_pentahedron.html", null ],
        [ "Structures.Solver3D.Elements.Solid.IsoP6NodePentahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p6_node_pentahedron.html", null ]
      ] ]
    ] ],
    [ "Structures.Solver3D.General.IStiffnessModifier", "interface_structures_1_1_solver3_d_1_1_general_1_1_i_stiffness_modifier.html", [
      [ "Structures.Solver3D.Elements.Linear.FrameStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_stiffness_modifiers.html", null ],
      [ "Structures.Solver3D.Elements.Planar.ShellStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stiffness_modifiers.html", null ],
      [ "Structures.Solver3D.Elements.Point.SpringStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_modifiers.html", null ],
      [ "Structures.Solver3D.Elements.Solid.SolidStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stiffness_modifiers.html", null ]
    ] ],
    [ "Structures.MathNetCustom.Spatial.Units.IUnit", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_unit.html", [
      [ "Structures.MathNetCustom.Spatial.Units.IAngleUnit", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_angle_unit.html", [
        [ "Structures.MathNetCustom.Spatial.Units.Degrees", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_degrees.html", null ],
        [ "Structures.MathNetCustom.Spatial.Units.Radians", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_radians.html", null ]
      ] ]
    ] ],
    [ "IXmlSerializable", null, [
      [ "Structures.MathNetCustom.Spatial.Euclidean.CoordinateSystem", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_coordinate_system.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Line3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Plane", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_plane.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Point3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Ray3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.UnitVector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Euclidean.Vector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html", null ],
      [ "Structures.MathNetCustom.Spatial.Units.Angle", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle.html", null ]
    ] ],
    [ "Structures.Materials.Elastic.Wood.LaminatedStrandLumberDesignValues", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values.html", null ],
    [ "Structures.Utilities.Reporting.Equations.LaTexVariable", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_la_tex_variable.html", [
      [ "Structures.Utilities.Reporting.Equations.ResultLimit", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_result_limit.html", null ]
    ] ],
    [ "Structures.Solver3D.Loads.LinearElementLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_load.html", [
      [ "Structures.Solver3D.Loads.LinearElementLineLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_line_load.html", [
        [ "Structures.Solver3D.Loads.LinearElementPercentWeightLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_percent_weight_load.html", [
          [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFramePercentWeightLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_percent_weight_load.html", null ]
        ] ],
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFrameLineLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_line_load.html", null ]
      ] ],
      [ "Structures.Solver3D.Loads.LinearElementPointLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_point_load.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFramePointLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_point_load.html", null ]
      ] ],
      [ "Structures.Solver3D.Loads.LinearElementThermalLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_thermal_load.html", [
        [ "Structures.SolverDesignModels.DesignElements.LinearElements.DesignFrameThermalLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_thermal_load.html", null ]
      ] ]
    ] ],
    [ "Structures.Solver3D.General.LineRestraintProperties", "class_structures_1_1_solver3_d_1_1_general_1_1_line_restraint_properties.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.LineSegment2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment2_d_extensions.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.AreaElements.LineSegment2DWrapper", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_line_segment2_d_wrapper.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.LineSegment3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.LinkedListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_linked_list_extensions.html", null ],
    [ "List", null, [
      [ "Structures.Utilities.PrimitiveGeometry.Point3DList", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_point3_d_list.html", null ],
      [ "Structures.Utilities.Svg.TooltipText", "class_structures_1_1_utilities_1_1_svg_1_1_tooltip_text.html", null ]
    ] ],
    [ "Structures.Utilities.Extensions.DotNetNative.ListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_list_extensions.html", null ],
    [ "Structures.Utilities.Loads.LoadCombination", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination.html", null ],
    [ "Structures.Utilities.Loads.LoadCombinationDeflectionCriteria", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html", null ],
    [ "Structures.Utilities.Loads.LoadPattern", "class_structures_1_1_utilities_1_1_loads_1_1_load_pattern.html", null ],
    [ "Structures.Solver3D.Elements.LocalAxes", "class_structures_1_1_solver3_d_1_1_elements_1_1_local_axes.html", null ],
    [ "Structures.Utilities.Logging.LogSolver3D", "class_structures_1_1_utilities_1_1_logging_1_1_log_solver3_d.html", null ],
    [ "Structures.Utilities.Modal.MassFromLoadPattern", "class_structures_1_1_utilities_1_1_modal_1_1_mass_from_load_pattern.html", null ],
    [ "Structures.Utilities.Modal.MassSource", "class_structures_1_1_utilities_1_1_modal_1_1_mass_source.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.MathExtensions", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_math_extensions.html", null ],
    [ "Structures.Utilities.DataStructures.QuadTree.Matrix", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_matrix.html", null ],
    [ "Structures.MathNetCustom.Spatial.Euclidean.Matrix2D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix2_d.html", null ],
    [ "Structures.MathNetCustom.Spatial.Euclidean.Matrix3D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix3_d.html", null ],
    [ "Structures.Utilities.MatrixLibrary.MatrixLibrary", "class_structures_1_1_utilities_1_1_matrix_library_1_1_matrix_library.html", null ],
    [ "Structures.Solver3D.Solver.MatrixSupport.MatrixOperation", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_matrix_operation.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.Extra.MechanicalDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mechanical_d_c_r.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.Extra.MemberDemands", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_member_demands.html", null ],
    [ "Structures.Utilities.TriangleNetHelper.MeshingOptions", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_meshing_options.html", null ],
    [ "Structures.Utilities.HttpRequests.Metadata", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.AreaElements.Profiles.MetalDeckProfile", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles_1_1_metal_deck_profile.html", null ],
    [ "Structures.Utilities.Containers.MinMaxBounds", "class_structures_1_1_utilities_1_1_containers_1_1_min_max_bounds.html", null ],
    [ "Structures.SolverDesignModels.Settings.ModalAnalysisSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html", null ],
    [ "Structures.Solver3D.Modal.ModalResults", "class_structures_1_1_solver3_d_1_1_modal_1_1_modal_results.html", null ],
    [ "Structures.Solver3D.Geometry.Model3D", "class_structures_1_1_solver3_d_1_1_geometry_1_1_model3_d.html", null ],
    [ "Structures.SolverDesignModels.Settings.ModelDefinitions", "class_structures_1_1_solver_design_models_1_1_settings_1_1_model_definitions.html", null ],
    [ "Structures.SolverDesignModels.Models.ModelExporter", "class_structures_1_1_solver_design_models_1_1_models_1_1_model_exporter.html", null ],
    [ "Structures.Solver3D.Modal.ModeShape", "class_structures_1_1_solver3_d_1_1_modal_1_1_mode_shape.html", null ],
    [ "Structures.Solver3D.Modal.NaturalFrequency", "class_structures_1_1_solver3_d_1_1_modal_1_1_natural_frequency.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.PointElements.NodalDrift", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_nodal_drift.html", null ],
    [ "Structures.Solver3D.Loads.NodalForce", "class_structures_1_1_solver3_d_1_1_loads_1_1_nodal_force.html", null ],
    [ "Structures.Solver3D.Elements.Linear.NodalReleases", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_nodal_releases.html", null ],
    [ "Structures.Solver3D.Nodes.NodalStress", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_stress.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.ObjectExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_object_extensions.html", null ],
    [ "ObservableCollection", null, [
      [ "Structures.Utilities.Containers.EnhancedObservableCollection< T >", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html", null ]
    ] ],
    [ "Structures.Utilities.HttpRequests.GeneralQueries.OpenTopo", "class_structures_1_1_utilities_1_1_http_requests_1_1_general_queries_1_1_open_topo.html", null ],
    [ "Structures.Utilities.HttpRequests.Parameters", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html", null ],
    [ "Structures.Utilities.Loads.Wind.ParapetPressure", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_parapet_pressure.html", null ],
    [ "Structures.Utilities.Units.Parsing.Parse", "class_structures_1_1_utilities_1_1_units_1_1_parsing_1_1_parse.html", null ],
    [ "Structures.MathNetCustom.Spatial.Parser", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_parser.html", null ],
    [ "Structures.Utilities.Loads.PatternResponseSpectrumProperties", "class_structures_1_1_utilities_1_1_loads_1_1_pattern_response_spectrum_properties.html", null ],
    [ "Structures.SolverDesignModels.Settings.PDeltaSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.PlaneExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.Point", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Point2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point2_d_extensions.html", null ],
    [ "Structures.Utilities.Geometry.Geometry3D.Point3D", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_point3_d.html", null ],
    [ "Structures.SolverDesignModels.Extensions.Point3DExtensions", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_point3_d_extensions.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Point3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point3_d_extensions.html", null ],
    [ "Structures.Utilities.Geometry.PointD", "class_structures_1_1_utilities_1_1_geometry_1_1_point_d.html", null ],
    [ "Structures.Solver3D.General.PointDisplacement", "class_structures_1_1_solver3_d_1_1_general_1_1_point_displacement.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.PointOctree< T >", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point_octree.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.PointOctree< AnalysisNode >", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point_octree.html", null ],
    [ "Structures.Utilities.DataStructures.QuadTree.PointQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_point_q_t.html", null ],
    [ "Structures.Utilities.PolyBool.PolyBoolExtensions", "class_structures_1_1_utilities_1_1_poly_bool_1_1_poly_bool_extensions.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Polygon2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Polygon3D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_polygon3_d.html", [
      [ "Structures.Utilities.PrimitiveGeometry.SimplePolygon2D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_simple_polygon2_d.html", null ]
    ] ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Polygon3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Polyline2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polyline2_d_extensions.html", null ],
    [ "Structures.Utilities.TriangleNetHelper.PreProcessor", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor.html", null ],
    [ "Structures.Utilities.TriangleNetHelper.PreProcessorQT", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor_q_t.html", null ],
    [ "Structures.Utilities.Reporting.Printer", "class_structures_1_1_utilities_1_1_reporting_1_1_printer.html", null ],
    [ "Structures.Utilities.Threading.ProcessorCores", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores.html", null ],
    [ "Structures.Utilities.Reporting.ProjectInformation", "class_structures_1_1_utilities_1_1_reporting_1_1_project_information.html", null ],
    [ "Structures.Utilities.DataStructures.QuadTree.QuadTree< T >", "class_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_quad_tree.html", null ],
    [ "Structures.Utilities.DataStructures.Octree.Ray", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_ray.html", null ],
    [ "Structures.Utilities.Reporting.Equations.RecordedCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_recorded_calculation.html", [
      [ "Structures.Utilities.Reporting.Equations.DesignCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_design_calculation.html", [
        [ "Structures.Utilities.Reporting.Equations.ExpressionlessDesignCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_expressionless_design_calculation.html", null ]
      ] ]
    ] ],
    [ "Structures.Utilities.DataStructures.QuadTree.Rect", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_rect.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.ReducedBeamSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_reduced_beam_section.html", null ],
    [ "Structures.Utilities.Reporting.ReferenceStandard", "class_structures_1_1_utilities_1_1_reporting_1_1_reference_standard.html", null ],
    [ "Structures.Utilities.HttpRequests.Request", "class_structures_1_1_utilities_1_1_http_requests_1_1_request.html", null ],
    [ "Structures.Solver3D.Properties.Resources", "class_structures_1_1_solver3_d_1_1_properties_1_1_resources.html", null ],
    [ "Structures.Utilities.HttpRequests.Response", "class_structures_1_1_utilities_1_1_http_requests_1_1_response.html", null ],
    [ "Structures.Utilities.Loads.ResponseSpectrum", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum.html", null ],
    [ "Structures.Utilities.Loads.ResponseSpectrumOrdinate", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum_ordinate.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.RigidEndZone", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_rigid_end_zone.html", null ],
    [ "Structures.Utilities.Reporting.Tabular.Row", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_row.html", null ],
    [ "Structures.Solver3D.General.SectionCut", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut.html", [
      [ "Structures.Solver3D.General.SectionCutSurface", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_surface.html", null ]
    ] ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.SectionProfile", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_section_profile.html", null ],
    [ "Structures.Solver3D.General.SectionStrip", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip.html", null ],
    [ "Structures.Solver3D.General.SectionStripForce", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip_force.html", null ],
    [ "Structures.Utilities.HttpRequests.SeismicDesignData", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_design_data.html", null ],
    [ "Structures.Utilities.Loads.Seismic.SeismicDesignResources", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_design_resources.html", null ],
    [ "Structures.Utilities.Loads.Seismic.SeismicForceResistingSystem", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_force_resisting_system.html", null ],
    [ "Structures.Utilities.HttpRequests.SeismicQueryUSGS", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_query_u_s_g_s.html", null ],
    [ "Structures.Utilities.Serialization.SerializeSettings", "class_structures_1_1_utilities_1_1_serialization_1_1_serialize_settings.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.ShapeMinMax", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect275d3bc18f27eb063e830427db2af41a.html", null ],
    [ "Structures.SolverDesignModels.DesignElements.LinearElements.CrossSections.ColdFormedSteel.ShearStiffenerProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect98b7b25066dd9cee2ae136378b631f2c.html", null ],
    [ "Structures.Solver3D.Elements.Planar.ShellBendingTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_bending_tensor.html", null ],
    [ "Structures.Solver3D.Elements.Planar.ShellMembraneTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_membrane_tensor.html", null ],
    [ "Structures.Solver3D.Elements.Planar.ShellStressTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress_tensor.html", null ],
    [ "Structures.Utilities.DataStructures.QuadTree.SizeQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_size_q_t.html", null ],
    [ "Structures.Utilities.Loads.Snow.ASCE_7_16.SnowLoadingParameters", "class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters.html", null ],
    [ "Structures.Solver3D.Elements.Solid.SolidStressTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress_tensor.html", null ],
    [ "Structures.Solver3D.General.SolverAnalysisOptions", "class_structures_1_1_solver3_d_1_1_general_1_1_solver_analysis_options.html", null ],
    [ "Structures.Solver3D.Elements.Point.SpringStiffnessValues", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_values.html", null ],
    [ "Structures.Utilities.MatrixLibrary.MatrixLibrary.StaticCondensationReturn", "struct_structures_1_1_utilities_1_1_matrix_library_1_1_matrix_library_1_1_static_condensation_return.html", null ],
    [ "Structures.SolverDesignModels.StaticFenix", "class_structures_1_1_solver_design_models_1_1_static_fenix.html", null ],
    [ "Structures.SolverDesignModels.Steel.SteelResources", "class_structures_1_1_solver_design_models_1_1_steel_1_1_steel_resources.html", null ],
    [ "Structures.Utilities.Compression.StringCompression", "class_structures_1_1_utilities_1_1_compression_1_1_string_compression.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.StringExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_string_extensions.html", null ],
    [ "Structures.Utilities.Loads.Seismic.StructuralHeightLimit", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_height_limit.html", null ],
    [ "Structures.Utilities.Loads.Seismic.StructuralSystemLimitations", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_system_limitations.html", null ],
    [ "Structures.Utilities.Reporting.Equations.SubCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_sub_calculation.html", null ],
    [ "Structures.Utilities.BuildingCodes.SupportedCodes", "class_structures_1_1_utilities_1_1_building_codes_1_1_supported_codes.html", null ],
    [ "Structures.Utilities.Svg.SvgDrawUtility", "class_structures_1_1_utilities_1_1_svg_1_1_svg_draw_utility.html", null ],
    [ "Structures.Utilities.Svg.SVGProperties", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html", null ],
    [ "Structures.Utilities.Reporting.Tabular.Table", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html", null ],
    [ "Structures.Utilities.Threading.TaskExtensions", "class_structures_1_1_utilities_1_1_threading_1_1_task_extensions.html", null ],
    [ "TaskScheduler", null, [
      [ "Structures.Utilities.Threading.LimitedConcurrencyLevelTaskScheduler", "class_structures_1_1_utilities_1_1_threading_1_1_limited_concurrency_level_task_scheduler.html", null ]
    ] ],
    [ "Structures.Utilities.Threading.ThreadExtension", "class_structures_1_1_utilities_1_1_threading_1_1_thread_extension.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< T >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< AnalysisNode >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< FrameElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< IGeneralElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< ImposedNodalDisplacement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< LinkElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< LoadCombination >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< NodalForce >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< OneNode3DSpringElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< ShellElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.Threading.ThreadSafeList< SolidElement >", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", null ],
    [ "Structures.Utilities.TriangleNetHelper.TriangleHelper", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_helper.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.TypeExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_type_extensions.html", null ],
    [ "Structures.MathNetCustom.Spatial.Units.UnitConverter", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_converter.html", null ],
    [ "Structures.MathNetCustom.Spatial.Units.UnitParser", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_parser.html", null ],
    [ "Structures.Utilities.Units.UnitSystem", "class_structures_1_1_utilities_1_1_units_1_1_unit_system.html", null ],
    [ "Structures.Utilities.Units.Conversion.Utilities", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_utilities.html", null ],
    [ "Structures.Utilities.Geometry.Vector", "class_structures_1_1_utilities_1_1_geometry_1_1_vector.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Vector2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector2_d_extensions.html", null ],
    [ "Structures.Utilities.Geometry.Geometry3D.Vector3D", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_vector3_d.html", null ],
    [ "Structures.Utilities.PrimitiveGeometry.Extensions.Vector3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector3_d_extensions.html", null ],
    [ "Structures.Utilities.DataStructures.QuadTree.VectorQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_vector_q_t.html", null ],
    [ "Structures.Utilities.Extensions.TriangleNet.VertexExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_triangle_net_1_1_vertex_extensions.html", null ],
    [ "Structures.Utilities.Loads.Wind.WallPressure", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure.html", null ],
    [ "Structures.Utilities.Loads.Wind.WallPressureTable", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure_table.html", null ],
    [ "Structures.SolverDesignModels.Steel.WidthToThickness", "class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness.html", null ],
    [ "Structures.Utilities.Loads.Wind.WindParameters", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wind_parameters.html", null ],
    [ "Structures.SolverDesignModels.Wood.WoodDesignResources", "class_structures_1_1_solver_design_models_1_1_wood_1_1_wood_design_resources.html", null ],
    [ "Structures.Materials.Elastic.Wood.WoodMaterialsResources", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_materials_resources.html", null ],
    [ "Structures.Materials.Elastic.Wood.WoodSpecies", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_species.html", null ],
    [ "Structures.Utilities.Extensions.DotNetNative.XFile", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_x_file.html", null ],
    [ "Structures.MathNetCustom.Spatial.XmlExt", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_xml_ext.html", null ]
];