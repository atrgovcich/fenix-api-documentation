var namespace_structures_1_1_utilities_1_1_charting =
[
    [ "Chart", "class_structures_1_1_utilities_1_1_charting_1_1_chart.html", "class_structures_1_1_utilities_1_1_charting_1_1_chart" ],
    [ "Chart2D", "class_structures_1_1_utilities_1_1_charting_1_1_chart2_d.html", "class_structures_1_1_utilities_1_1_charting_1_1_chart2_d" ],
    [ "AxisScale", "namespace_structures_1_1_utilities_1_1_charting.html#ae718d0381278e4b4594637808283a3c2", [
      [ "Normal", "namespace_structures_1_1_utilities_1_1_charting.html#ae718d0381278e4b4594637808283a3c2a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "Logarithmic", "namespace_structures_1_1_utilities_1_1_charting.html#ae718d0381278e4b4594637808283a3c2a2f8e38ec1a5832670c5011a71603c929", null ]
    ] ],
    [ "InterpolationType", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6", [
      [ "None", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "CanonicalSpline", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6a83afee5528acef68a4da08d644105c2e", null ],
      [ "CatmullRomSpline", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6ad4bb969c694ef9ad4d0eb038decf5e0e", null ],
      [ "UniformCatmullRomSpline", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6a1db6073f6361ae64ede982a6186d3a99", null ],
      [ "ChordalCatmullRomSpline", "namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6a9da618abf48879b8301c4cb9a974dc05", null ]
    ] ]
];