var class_structures_1_1_utilities_1_1_http_requests_1_1_parameters =
[
    [ "latitude", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html#a8cfeb641c968fbc92fb0abfbbb0cc273", null ],
    [ "longitude", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html#ad6d872dd29f590cc2423e2b54d8e1c6f", null ],
    [ "riskCategory", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html#a526f50d497e6c85b46087bfd516922b3", null ],
    [ "siteClass", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html#a522fe3de9a47a3e0fd3a03e3e293fbfd", null ],
    [ "title", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html#afc49bdb16dda3f3b9f8a5624d47cf917", null ]
];