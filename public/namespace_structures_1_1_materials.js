var namespace_structures_1_1_materials =
[
    [ "Elastic", "namespace_structures_1_1_materials_1_1_elastic.html", "namespace_structures_1_1_materials_1_1_elastic" ],
    [ "General", "namespace_structures_1_1_materials_1_1_general.html", "namespace_structures_1_1_materials_1_1_general" ],
    [ "Inelastic", "namespace_structures_1_1_materials_1_1_inelastic.html", "namespace_structures_1_1_materials_1_1_inelastic" ],
    [ "Properties", "namespace_structures_1_1_materials_1_1_properties.html", null ],
    [ "IGenericMaterial", "interface_structures_1_1_materials_1_1_i_generic_material.html", "interface_structures_1_1_materials_1_1_i_generic_material" ],
    [ "MaterialType", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155", [
      [ "Aluminum", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a114e57c0cc6efff793a1dd8278193a5c", null ],
      [ "UnconfinedConcrete", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a24a37b256f95a62783b05d141090b76b", null ],
      [ "Masonry", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155aaa47777dc638b9a23ba7265627910b14", null ],
      [ "Steel", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a4668112d7c5f80a73a826dd8150989df", null ],
      [ "Wood", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a6e4dd7ce4ea3c1d4a90edb289e22da98", null ],
      [ "Glulam", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a8d7661d95022192a6fdeebebde580029", null ],
      [ "GeneralIsotropic", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a46ae5d1bce3d9746b93a68fd47918905", null ],
      [ "GeneralOrthotropic", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a15471b65861cd17d4b4940268b09968e", null ],
      [ "PrestressingSteel", "namespace_structures_1_1_materials.html#aeb4f4677e11eea5a0fd16bdf68c52155a143012d7b52103aa1630c9da69aa6bbe", null ]
    ] ]
];