var class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings =
[
    [ "ColorDcrCells", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#a03830b50b9483b09e2ca1911582ba667", null ],
    [ "DetailedCalculationElements", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#af9e18145e3ceb8a7d6843f1777096c57", null ],
    [ "IncludeDeflectionDesign", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#a11c1b64f82b463ccd271a7617a0d6634", null ],
    [ "IncludeDetailingDesign", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#aa45c2f4ed2321908c75efdc2fa3e1d34", null ],
    [ "IncludeFireDesign", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#afe1c4e3e854cf5956fdea4b09b1ca969", null ],
    [ "IncludeMaterialSummary", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#a5fbaf09307391fd4025b7499275e46b5", null ],
    [ "IncludeServiceDesign", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#aafa8387deb9f72b49469d0819c058da1", null ],
    [ "IncludeStrengthDesign", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#a4dc94834fd4969f936d31d1155053c0a", null ],
    [ "LoadCombinationsForDetailedCalculations", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#afe21d2c1157c0872cc92a242c6189d96", null ],
    [ "UseDesignElementNames", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html#a114fb9f4635b53a5e0d9826abdd7baca", null ]
];