var namespace_structures_1_1_solver_design_models_1_1_settings =
[
    [ "DesignReportSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings" ],
    [ "ModalAnalysisSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings" ],
    [ "ModelDefinitions", "class_structures_1_1_solver_design_models_1_1_settings_1_1_model_definitions.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_model_definitions" ],
    [ "PDeltaSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings" ]
];