var namespace_structures_1_1_utilities_1_1_graphics =
[
    [ "ColorGradient", "class_structures_1_1_utilities_1_1_graphics_1_1_color_gradient.html", "class_structures_1_1_utilities_1_1_graphics_1_1_color_gradient" ],
    [ "IColorizable", "interface_structures_1_1_utilities_1_1_graphics_1_1_i_colorizable.html", "interface_structures_1_1_utilities_1_1_graphics_1_1_i_colorizable" ],
    [ "ColorGradientPreset", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590", [
      [ "FenixBiColor", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590a6e0b7eb69d905c0fedf4de1a50e7f98a", null ],
      [ "FenixTriColor", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590ad02d851f12776e27397ebb98c12e0c22", null ],
      [ "BlackPink", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590a7ee8dca043e1cb800fb10c02ec7e0e94", null ],
      [ "BlueGreenRed", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590aa2f3944b38408ab0741a120ebda7881d", null ],
      [ "Turbo", "namespace_structures_1_1_utilities_1_1_graphics.html#a719a30a33fc0df42405eb1d9875b3590a6f53bfe04e78da893ba0c4f35ba6847e", null ]
    ] ]
];