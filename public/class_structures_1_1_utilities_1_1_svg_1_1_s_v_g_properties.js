var class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties =
[
    [ "SVGProperties", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a7dba63e88f747657527313c856500f5f", null ],
    [ "BoxHeight", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a2b16ba500abd92ce6b959d02a9359fcb", null ],
    [ "BoxWidth", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#ad10e577e7512dd3d9facef1938244c27", null ],
    [ "LegendLocation", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#ad97ae41240a9c4f272b7ed711c8d7d44", null ],
    [ "MinimumX", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a3c73d5090627616e62f9bde343378fd5", null ],
    [ "MinimumY", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a9ace30f3e98ef8921cd5bb3e5b1053b4", null ],
    [ "Padding", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a298208856cbc2c161389fc9c3946b802", null ],
    [ "ScaleFactor", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html#a51e78c1dad07bd5c2067602634d32be8", null ]
];