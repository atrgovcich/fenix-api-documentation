var interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape =
[
    [ "BoundingRectangle", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html#a1c61619bda3249433eded7b689541968", null ],
    [ "Edges", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html#a1a5617dc1dc7e0cfcd015faa21003612", null ],
    [ "Hole", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html#a33e4b6b37dbe8fe8be58c03689779a50", null ],
    [ "IsClosed", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html#a3a35c25546bcc627563e1de7f5b76562", null ],
    [ "Vertices", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html#ab2a7d26bb802466bb45382665649e1d0", null ]
];