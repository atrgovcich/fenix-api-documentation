var namespace_structures_1_1_utilities_1_1_geometry =
[
    [ "Geometry3D", "namespace_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d.html", "namespace_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d" ],
    [ "CircleDefinition", "struct_structures_1_1_utilities_1_1_geometry_1_1_circle_definition.html", "struct_structures_1_1_utilities_1_1_geometry_1_1_circle_definition" ],
    [ "Edge", "class_structures_1_1_utilities_1_1_geometry_1_1_edge.html", "class_structures_1_1_utilities_1_1_geometry_1_1_edge" ],
    [ "GenericCircle", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle" ],
    [ "GenericClosedShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape" ],
    [ "GenericPoint", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_point.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_point" ],
    [ "GenericPolygon", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polygon.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polygon" ],
    [ "GenericPolyline", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polyline.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polyline" ],
    [ "GenericShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_shape.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_shape" ],
    [ "GeometryFunctions", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry_functions.html", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry_functions" ],
    [ "IGenericClosedShape", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_closed_shape.html", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_closed_shape" ],
    [ "IGenericGeometry", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_geometry.html", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_geometry" ],
    [ "PointD", "class_structures_1_1_utilities_1_1_geometry_1_1_point_d.html", "class_structures_1_1_utilities_1_1_geometry_1_1_point_d" ],
    [ "Vector", "class_structures_1_1_utilities_1_1_geometry_1_1_vector.html", "class_structures_1_1_utilities_1_1_geometry_1_1_vector" ],
    [ "SideOfLine", "namespace_structures_1_1_utilities_1_1_geometry.html#a5c0d05d952a7aba9fc5cfdbc60975f21", [
      [ "Left", "namespace_structures_1_1_utilities_1_1_geometry.html#a5c0d05d952a7aba9fc5cfdbc60975f21a945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Right", "namespace_structures_1_1_utilities_1_1_geometry.html#a5c0d05d952a7aba9fc5cfdbc60975f21a92b09c7c48c520c3c55e497875da437c", null ],
      [ "Coincident", "namespace_structures_1_1_utilities_1_1_geometry.html#a5c0d05d952a7aba9fc5cfdbc60975f21a81126870f8ecd751b93a0f51d10a8945", null ]
    ] ]
];