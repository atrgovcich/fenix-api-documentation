var struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape =
[
    [ "Equals", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#ac1a2e82d3767608a848151c452fea6fe", null ],
    [ "Equals", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#a470b54f3f6e3d916d23ebe33176da36f", null ],
    [ "GetHashCode", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#adc6e8ab5fdbddf333f20814077cd0826", null ],
    [ "BuiltUpSectionScrewLayout", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#a1f71f0a63196ac38e03c1cd93cf622e0", null ],
    [ "Designation", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#a0043956d6b73dc68badb8343177d5cd7", null ],
    [ "HoleDesignation", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html#a61cfe07de512ba57bc0fe2e0fe149fd5", null ]
];