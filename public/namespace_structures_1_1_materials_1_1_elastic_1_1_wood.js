var namespace_structures_1_1_materials_1_1_elastic_1_1_wood =
[
    [ "CLTLayer", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_c_l_t_layer.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_c_l_t_layer" ],
    [ "CrossLaminatedTimberProduct", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_cross_laminated_timber_product.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_cross_laminated_timber_product" ],
    [ "GluedLaminatedMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_glued_laminated_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_glued_laminated_material" ],
    [ "GradingAgencyTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_agency_type_functions.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_agency_type_functions" ],
    [ "GradingMethodTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_method_type_functions.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_method_type_functions" ],
    [ "LaminatedStrandLumberDesignValues", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values" ],
    [ "LaminatedStrandLumberMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_material" ],
    [ "SolidWoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_solid_wood_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_solid_wood_material" ],
    [ "WoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_material" ],
    [ "WoodMaterialsResources", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_materials_resources.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_materials_resources" ],
    [ "WoodSpecies", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_species.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_species" ],
    [ "GradingAgencyType", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1deca", [
      [ "WWPA", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaa5343063ea08d95bd826c3b91d76f6491", null ],
      [ "WCLIB", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaac7f4465110a0765999351f5dd8d65ffc", null ],
      [ "NELMA", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaa0193a8caabec9b81fa4553579418d6d5", null ],
      [ "NLGA", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaab9134a3e282a5b8de759e3c65878cc58", null ],
      [ "NSLB", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaa9f7712eb19cec257c25eee5e721c6586", null ],
      [ "SPIB", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a78014c8914dc97d2cd1fd1da8ee1decaa8a1882b55007b7c317df43c535c1b516", null ]
    ] ],
    [ "GradingMethodType", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a588976e29896e47942a0783b5bb80b40", [
      [ "VisuallyGraded", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a588976e29896e47942a0783b5bb80b40adb52e8fb1a6ddcc098fc51af18fc7605", null ],
      [ "MechanicallyStressRated", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a588976e29896e47942a0783b5bb80b40ad9e25f44f8064ec98947161715a9bcb3", null ]
    ] ],
    [ "LayerOrientation", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#ae42edfe3b0aa1f95d8f4bca8bd12f0a7", [
      [ "Parallel", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#ae42edfe3b0aa1f95d8f4bca8bd12f0a7a98402eecfbcefc336954458a01752131", null ],
      [ "Perpendicular", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#ae42edfe3b0aa1f95d8f4bca8bd12f0a7acd8b3187c4074a4bb810f1fef79fd52a", null ]
    ] ],
    [ "MoistureConditionType", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a93b0a859c115f9ec7093ef7b07a96b7d", [
      [ "Dry", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a93b0a859c115f9ec7093ef7b07a96b7dabbdc4863db2ec1f5a8ec579d53738c72", null ],
      [ "Wet", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#a93b0a859c115f9ec7093ef7b07a96b7daae789b866db2fac36cf644f4cdba1267", null ]
    ] ],
    [ "SawnLumberType", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#af86c5a72adf40911f613fa43e822e1c8", [
      [ "DimensionedLumber", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#af86c5a72adf40911f613fa43e822e1c8af4b038bbd1c146501a6e7c60fa563eeb", null ],
      [ "Timbers", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html#af86c5a72adf40911f613fa43e822e1c8aa93cb792e304fa6d80c0b923d13a72b3", null ]
    ] ]
];