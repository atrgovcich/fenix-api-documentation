var namespace_structures_1_1_utilities_1_1_primitive_geometry =
[
    [ "Extensions", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions" ],
    [ "Helpers", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers.html", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers" ],
    [ "GeometryHelper", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_geometry_helper.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_geometry_helper" ],
    [ "InclinedRotations", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations" ],
    [ "Point3DList", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_point3_d_list.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_point3_d_list" ],
    [ "Polygon3D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_polygon3_d.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_polygon3_d" ],
    [ "SimplePolygon2D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_simple_polygon2_d.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_simple_polygon2_d" ]
];