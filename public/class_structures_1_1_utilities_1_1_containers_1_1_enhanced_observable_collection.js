var class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection =
[
    [ "EnhancedObservableCollection", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#a3dbcd465b2bdf825b3f38901ead155ca", null ],
    [ "EnhancedObservableCollection", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#a2c6c797634a9d5ddf5efafc4c57c978b", null ],
    [ "EnhancedObservableCollection", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#ae64353f1065bfd444cf688dd3cd3ffee", null ],
    [ "InsertItem", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#ad60d42eb9ce40e0ccf93071313ef7828", null ],
    [ "RemoveItem", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#a53b62a0bda2bb654ad27cf8a0731bda3", null ],
    [ "IsCollectionChangedOnChildChange", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#a27c851360c7acdb21ad5fc85df7ead55", null ],
    [ "ChildChanged", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html#aa8d832d80926058ad4ae19d8cac63060", null ]
];