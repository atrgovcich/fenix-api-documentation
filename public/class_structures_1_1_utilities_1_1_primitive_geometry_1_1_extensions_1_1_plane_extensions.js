var class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions =
[
    [ "FromManyPointsBestFit", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html#a0a72bfa3f899c236a1ab2fd19b59765b", null ],
    [ "GetInclinedRotations", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html#a00736e4f1d4e8e5c9ce843e7e08924c7", null ],
    [ "NormalPointsUpward", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html#a9ea775ed3f106c0836f4ef91fc648016", null ]
];