var namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles =
[
    [ "MetalDeckProfile", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles_1_1_metal_deck_profile.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles_1_1_metal_deck_profile" ],
    [ "MetalDeckProfileType", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles.html#a77dab7718d0cd16ffd52d6b9871b9634", [
      [ "Roof", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles.html#a77dab7718d0cd16ffd52d6b9871b9634a64230583b0483604ea026c80b127f16a", null ],
      [ "Floor", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles.html#a77dab7718d0cd16ffd52d6b9871b9634af3f6d0343d56ce88ce7958170ed05cb3", null ]
    ] ]
];