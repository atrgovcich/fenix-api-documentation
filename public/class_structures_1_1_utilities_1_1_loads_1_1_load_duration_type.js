var class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type =
[
    [ "FromDescription", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a1a7be845159c855bcf881a856d9c89a0", null ],
    [ "Impact", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a176da8c193d92594e7a54b4ad39ffeb1", null ],
    [ "Permanent", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a2b6df774f5134c0d11a9a839f6f1e3c8", null ],
    [ "SevenDays", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#aae9defef35d5906afdc2149efc24d7d8", null ],
    [ "TenMinutes", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a99f38ef51129ac65f1ccfe61fddc207b", null ],
    [ "TenYears", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a4ac1582f42246daee8ae25634e9e301f", null ],
    [ "TwoMonths", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a222ca3c43cff5d01fa738ef7303407fa", null ],
    [ "Cd", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html#a3204567248b50c106f613f2b52f87a59", null ]
];