var namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16 =
[
    [ "SnowLoadingParameters", "class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters.html", "class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters" ],
    [ "RoofExposureCategory", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a6ff237e235d4cca519abfa6247ed2d85", [
      [ "FullyExposed", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a6ff237e235d4cca519abfa6247ed2d85a14a9cc302b5b76d8e7822f8b09ef825a", null ],
      [ "PartiallyExposed", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a6ff237e235d4cca519abfa6247ed2d85ac7984b52c56578b8bec58721067840b2", null ],
      [ "Sheltered", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a6ff237e235d4cca519abfa6247ed2d85ae95f837890ad66c852e216772c06f85a", null ]
    ] ],
    [ "RoofSurface", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a5108aa974a4c356f01fea015c0891aaa", [
      [ "UnobstructedSliperySurfaces", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a5108aa974a4c356f01fea015c0891aaaa31f48a597705ca6bfa613ad38cfafe74", null ],
      [ "AllOtherSurfaces", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a5108aa974a4c356f01fea015c0891aaaa68f2b7c2517faf91547280e465df2345", null ]
    ] ],
    [ "ThermalCondition", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71", [
      [ "AllOtherStructures", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71a4b3abd12f18d16302976d38ded46e66f", null ],
      [ "StructuresJustAboveFreezingWithColdVentilatedRoofs", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71aeeea079905d77f1491a944b10225e69b", null ],
      [ "UnheatedAndOpenAirStructures", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71a7e630158bd9a5a69d64c141abeed3ff1", null ],
      [ "FreezerBuilding", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71a1b060d59afa231e95d181c41e3598198", null ],
      [ "ContinuouslyHeatedGreenhouses", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a3ea67ce9686148a477a5d119d0dbce71a7a612a727fc82a39b1dd5f067ccd4ea4", null ]
    ] ]
];