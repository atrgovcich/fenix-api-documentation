var annotated_dup =
[
    [ "Structures", "namespace_structures.html", [
      [ "Materials", "namespace_structures_1_1_materials.html", [
        [ "Elastic", "namespace_structures_1_1_materials_1_1_elastic.html", [
          [ "Wood", "namespace_structures_1_1_materials_1_1_elastic_1_1_wood.html", [
            [ "CLTLayer", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_c_l_t_layer.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_c_l_t_layer" ],
            [ "CrossLaminatedTimberProduct", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_cross_laminated_timber_product.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_cross_laminated_timber_product" ],
            [ "GluedLaminatedMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_glued_laminated_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_glued_laminated_material" ],
            [ "GradingAgencyTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_agency_type_functions.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_agency_type_functions" ],
            [ "GradingMethodTypeFunctions", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_method_type_functions.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_grading_method_type_functions" ],
            [ "LaminatedStrandLumberDesignValues", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values" ],
            [ "LaminatedStrandLumberMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_material" ],
            [ "SolidWoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_solid_wood_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_solid_wood_material" ],
            [ "WoodMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_material" ],
            [ "WoodMaterialsResources", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_materials_resources.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_materials_resources" ],
            [ "WoodSpecies", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_species.html", "class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_wood_species" ]
          ] ],
          [ "ElasticMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_elastic_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_elastic_material" ],
          [ "IElasticMaterial", "interface_structures_1_1_materials_1_1_elastic_1_1_i_elastic_material.html", "interface_structures_1_1_materials_1_1_elastic_1_1_i_elastic_material" ],
          [ "IsotropicMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_isotropic_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_isotropic_material" ],
          [ "OrthotropicMaterial", "class_structures_1_1_materials_1_1_elastic_1_1_orthotropic_material.html", "class_structures_1_1_materials_1_1_elastic_1_1_orthotropic_material" ]
        ] ],
        [ "General", "namespace_structures_1_1_materials_1_1_general.html", [
          [ "Constants", "class_structures_1_1_materials_1_1_general_1_1_constants.html", "class_structures_1_1_materials_1_1_general_1_1_constants" ]
        ] ],
        [ "Inelastic", "namespace_structures_1_1_materials_1_1_inelastic.html", [
          [ "Concrete", "namespace_structures_1_1_materials_1_1_inelastic_1_1_concrete.html", [
            [ "CircularConfinementProperties", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_circular_confinement_properties.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_circular_confinement_properties" ],
            [ "ConfinedConcreteMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confined_concrete_material.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confined_concrete_material" ],
            [ "ConfinementProperties", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confinement_properties.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confinement_properties" ],
            [ "HsuHighStrengthConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_hsu_high_strength_concrete.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_hsu_high_strength_concrete" ],
            [ "InelasticConcreteMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_inelastic_concrete_material.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_inelastic_concrete_material" ],
            [ "ManderConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_concrete.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_concrete" ],
            [ "ManderConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_confined_concrete.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_mander_confined_concrete" ],
            [ "PiecewiseLinearConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_piecewise_linear_confined_concrete.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_piecewise_linear_confined_concrete" ],
            [ "RestrepoConfinedConcrete", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_restrepo_confined_concrete.html", "class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_restrepo_confined_concrete" ]
          ] ],
          [ "Steel", "namespace_structures_1_1_materials_1_1_inelastic_1_1_steel.html", [
            [ "InelasticSteelMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_inelastic_steel_material.html", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_inelastic_steel_material" ],
            [ "KentSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_kent_steel.html", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_kent_steel" ],
            [ "ManderSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_mander_steel.html", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_mander_steel" ],
            [ "MenegottoPintoSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_menegotto_pinto_steel.html", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_menegotto_pinto_steel" ],
            [ "ModifiedRambergOsgoodSteel", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_modified_ramberg_osgood_steel.html", "class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_modified_ramberg_osgood_steel" ]
          ] ],
          [ "InelasticMaterial", "class_structures_1_1_materials_1_1_inelastic_1_1_inelastic_material.html", "class_structures_1_1_materials_1_1_inelastic_1_1_inelastic_material" ]
        ] ],
        [ "IGenericMaterial", "interface_structures_1_1_materials_1_1_i_generic_material.html", "interface_structures_1_1_materials_1_1_i_generic_material" ]
      ] ],
      [ "MathNetCustom", "namespace_structures_1_1_math_net_custom.html", [
        [ "Spatial", "namespace_structures_1_1_math_net_custom_1_1_spatial.html", [
          [ "Euclidean", "namespace_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean.html", [
            [ "Circle3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_circle3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_circle3_d" ],
            [ "CoordinateSystem", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_coordinate_system.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_coordinate_system" ],
            [ "EulerAngles", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_euler_angles.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_euler_angles" ],
            [ "Line3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line3_d" ],
            [ "LineSegment3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line_segment3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_line_segment3_d" ],
            [ "Matrix2D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix2_d.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix2_d" ],
            [ "Matrix3D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix3_d.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_matrix3_d" ],
            [ "Plane", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_plane.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_plane" ],
            [ "Point3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d" ],
            [ "PolyLine3D", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_poly_line3_d.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_poly_line3_d" ],
            [ "Quaternion", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_quaternion.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_quaternion" ],
            [ "Ray3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d" ],
            [ "UnitVector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_unit_vector3_d" ],
            [ "Vector3D", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d" ]
          ] ],
          [ "Internals", "namespace_structures_1_1_math_net_custom_1_1_spatial_1_1_internals.html", [
            [ "ConvexHull", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_internals_1_1_convex_hull.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_internals_1_1_convex_hull" ]
          ] ],
          [ "Units", "namespace_structures_1_1_math_net_custom_1_1_spatial_1_1_units.html", [
            [ "Angle", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle" ],
            [ "AngleUnit", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle_unit.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_angle_unit" ],
            [ "Degrees", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_degrees.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_degrees" ],
            [ "IAngleUnit", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_angle_unit.html", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_angle_unit" ],
            [ "IUnit", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_unit.html", "interface_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_i_unit" ],
            [ "Radians", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_radians.html", "struct_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_radians" ],
            [ "UnitConverter", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_converter.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_converter" ],
            [ "UnitParser", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_parser.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_units_1_1_unit_parser" ]
          ] ],
          [ "Parser", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_parser.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_parser" ],
          [ "XmlExt", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_xml_ext.html", "class_structures_1_1_math_net_custom_1_1_spatial_1_1_xml_ext" ]
        ] ]
      ] ],
      [ "Solver3D", "namespace_structures_1_1_solver3_d.html", [
        [ "CoordinateConversions", "namespace_structures_1_1_solver3_d_1_1_coordinate_conversions.html", [
          [ "CoordinateSystemOperations", "class_structures_1_1_solver3_d_1_1_coordinate_conversions_1_1_coordinate_system_operations.html", "class_structures_1_1_solver3_d_1_1_coordinate_conversions_1_1_coordinate_system_operations" ]
        ] ],
        [ "Elements", "namespace_structures_1_1_solver3_d_1_1_elements.html", [
          [ "Linear", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_linear.html", [
            [ "CrossSection", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_cross_section.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_cross_section" ],
            [ "DegreeOfFreedomRelease", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_degree_of_freedom_release.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_degree_of_freedom_release" ],
            [ "FrameElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element" ],
            [ "FrameElementEndForce", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_force.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_force" ],
            [ "FrameElementEndRelease", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_release.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element_end_release" ],
            [ "FrameStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_stiffness_modifiers.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_stiffness_modifiers" ],
            [ "GlobalLinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_global_link_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_global_link_element" ],
            [ "LinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element" ],
            [ "LinkElementEndForce", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element_end_force.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element_end_force" ],
            [ "LocalLinkElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_local_link_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_local_link_element" ],
            [ "NodalReleases", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_nodal_releases.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_nodal_releases" ],
            [ "TimoshenkoBeamElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_timoshenko_beam_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_timoshenko_beam_element" ]
          ] ],
          [ "Planar", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_planar.html", [
            [ "Quadrilateral", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral.html", [
              [ "DkmqFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkmq_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkmq_formulation" ],
              [ "DkqFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkq_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_dkq_formulation" ],
              [ "QbdFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_qbd_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_qbd_formulation" ],
              [ "QuadBendingFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_bending_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_bending_formulation" ],
              [ "QuadMembraneFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_membrane_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_membrane_formulation" ],
              [ "QuadShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_shell_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral_1_1_quad_shell_element" ]
            ] ],
            [ "Triangular", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular.html", [
              [ "DkmtFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkmt_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkmt_formulation" ],
              [ "DktFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkt_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_dkt_formulation" ],
              [ "OptFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_opt_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_opt_formulation" ],
              [ "TriangularBendingFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_bending_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_bending_formulation" ],
              [ "TriangularMembraneFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_membrane_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_membrane_formulation" ],
              [ "TriangularShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_shell_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_triangular_1_1_triangular_shell_element" ]
            ] ],
            [ "IBendingFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_bending_formulation.html", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_bending_formulation" ],
            [ "IMembraneFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_membrane_formulation.html", "interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_membrane_formulation" ],
            [ "ShellBendingTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_bending_tensor.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_bending_tensor" ],
            [ "ShellElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_element" ],
            [ "ShellMembraneTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_membrane_tensor.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_membrane_tensor" ],
            [ "ShellStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stiffness_modifiers.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stiffness_modifiers" ],
            [ "ShellStress", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress" ],
            [ "ShellStressTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress_tensor.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress_tensor" ]
          ] ],
          [ "Point", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_point.html", [
            [ "AreaSpringProperties", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_area_spring_properties.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_area_spring_properties" ],
            [ "OneNode3DSpringElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_one_node3_d_spring_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_one_node3_d_spring_element" ],
            [ "SpringStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_modifiers.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_modifiers" ],
            [ "SpringStiffnessValues", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_values.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_spring_stiffness_values" ]
          ] ],
          [ "Solid", "namespace_structures_1_1_solver3_d_1_1_elements_1_1_solid.html", [
            [ "HexahedralFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedral_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedral_formulation" ],
            [ "HexahedronElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedron_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_hexahedron_element" ],
            [ "ISolidFormulation", "interface_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_i_solid_formulation.html", "interface_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_i_solid_formulation" ],
            [ "IsoP15NodePentahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p15_node_pentahedron.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p15_node_pentahedron" ],
            [ "IsoP20NodeHexahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p20_node_hexahedron.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p20_node_hexahedron" ],
            [ "IsoP6NodePentahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p6_node_pentahedron.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p6_node_pentahedron" ],
            [ "IsoP8NodeHexahedron", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p8_node_hexahedron.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p8_node_hexahedron" ],
            [ "PentahedralFormulation", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedral_formulation.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedral_formulation" ],
            [ "PentahedronElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedron_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_pentahedron_element" ],
            [ "SolidElement", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_element.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_element" ],
            [ "SolidStiffnessModifiers", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stiffness_modifiers.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stiffness_modifiers" ],
            [ "SolidStress", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress" ],
            [ "SolidStressTensor", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress_tensor.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress_tensor" ]
          ] ],
          [ "ElementResult", "class_structures_1_1_solver3_d_1_1_elements_1_1_element_result.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_element_result" ],
          [ "IGeneralElement", "interface_structures_1_1_solver3_d_1_1_elements_1_1_i_general_element.html", "interface_structures_1_1_solver3_d_1_1_elements_1_1_i_general_element" ],
          [ "LocalAxes", "class_structures_1_1_solver3_d_1_1_elements_1_1_local_axes.html", "class_structures_1_1_solver3_d_1_1_elements_1_1_local_axes" ]
        ] ],
        [ "General", "namespace_structures_1_1_solver3_d_1_1_general.html", [
          [ "AnalysisResult", "class_structures_1_1_solver3_d_1_1_general_1_1_analysis_result.html", "class_structures_1_1_solver3_d_1_1_general_1_1_analysis_result" ],
          [ "AutoElfCalculationVariables", "class_structures_1_1_solver3_d_1_1_general_1_1_auto_elf_calculation_variables.html", "class_structures_1_1_solver3_d_1_1_general_1_1_auto_elf_calculation_variables" ],
          [ "DiaphragmDefinition", "class_structures_1_1_solver3_d_1_1_general_1_1_diaphragm_definition.html", "class_structures_1_1_solver3_d_1_1_general_1_1_diaphragm_definition" ],
          [ "IStiffnessModifier", "interface_structures_1_1_solver3_d_1_1_general_1_1_i_stiffness_modifier.html", null ],
          [ "LineRestraintProperties", "class_structures_1_1_solver3_d_1_1_general_1_1_line_restraint_properties.html", "class_structures_1_1_solver3_d_1_1_general_1_1_line_restraint_properties" ],
          [ "PointDisplacement", "class_structures_1_1_solver3_d_1_1_general_1_1_point_displacement.html", "class_structures_1_1_solver3_d_1_1_general_1_1_point_displacement" ],
          [ "SectionCut", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut.html", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut" ],
          [ "SectionCutForce", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_force.html", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_force" ],
          [ "SectionCutSurface", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_surface.html", "class_structures_1_1_solver3_d_1_1_general_1_1_section_cut_surface" ],
          [ "SectionStrip", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip.html", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip" ],
          [ "SectionStripForce", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip_force.html", "class_structures_1_1_solver3_d_1_1_general_1_1_section_strip_force" ],
          [ "SolverAnalysisOptions", "class_structures_1_1_solver3_d_1_1_general_1_1_solver_analysis_options.html", "class_structures_1_1_solver3_d_1_1_general_1_1_solver_analysis_options" ]
        ] ],
        [ "Geometry", "namespace_structures_1_1_solver3_d_1_1_geometry.html", [
          [ "DegreeOfFreedomFixity", "class_structures_1_1_solver3_d_1_1_geometry_1_1_degree_of_freedom_fixity.html", "class_structures_1_1_solver3_d_1_1_geometry_1_1_degree_of_freedom_fixity" ],
          [ "Model3D", "class_structures_1_1_solver3_d_1_1_geometry_1_1_model3_d.html", "class_structures_1_1_solver3_d_1_1_geometry_1_1_model3_d" ]
        ] ],
        [ "Loads", "namespace_structures_1_1_solver3_d_1_1_loads.html", [
          [ "BodyForce", "class_structures_1_1_solver3_d_1_1_loads_1_1_body_force.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_body_force" ],
          [ "ImposedNodalDisplacement", "class_structures_1_1_solver3_d_1_1_loads_1_1_imposed_nodal_displacement.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_imposed_nodal_displacement" ],
          [ "LinearElementLineLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_line_load.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_line_load" ],
          [ "LinearElementLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_load.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_load" ],
          [ "LinearElementPercentWeightLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_percent_weight_load.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_percent_weight_load" ],
          [ "LinearElementPointLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_point_load.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_point_load" ],
          [ "LinearElementThermalLoad", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_thermal_load.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_linear_element_thermal_load" ],
          [ "NodalForce", "class_structures_1_1_solver3_d_1_1_loads_1_1_nodal_force.html", "class_structures_1_1_solver3_d_1_1_loads_1_1_nodal_force" ]
        ] ],
        [ "Modal", "namespace_structures_1_1_solver3_d_1_1_modal.html", [
          [ "ModalResults", "class_structures_1_1_solver3_d_1_1_modal_1_1_modal_results.html", "class_structures_1_1_solver3_d_1_1_modal_1_1_modal_results" ],
          [ "ModeShape", "class_structures_1_1_solver3_d_1_1_modal_1_1_mode_shape.html", "class_structures_1_1_solver3_d_1_1_modal_1_1_mode_shape" ],
          [ "NaturalFrequency", "class_structures_1_1_solver3_d_1_1_modal_1_1_natural_frequency.html", "class_structures_1_1_solver3_d_1_1_modal_1_1_natural_frequency" ]
        ] ],
        [ "Nodes", "namespace_structures_1_1_solver3_d_1_1_nodes.html", [
          [ "AnalysisNode", "class_structures_1_1_solver3_d_1_1_nodes_1_1_analysis_node.html", "class_structures_1_1_solver3_d_1_1_nodes_1_1_analysis_node" ],
          [ "NodalDisplacement", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_displacement.html", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_displacement" ],
          [ "NodalReaction", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_reaction.html", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_reaction" ],
          [ "NodalStress", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_stress.html", "class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_stress" ]
        ] ],
        [ "Properties", "namespace_structures_1_1_solver3_d_1_1_properties.html", [
          [ "Resources", "class_structures_1_1_solver3_d_1_1_properties_1_1_resources.html", "class_structures_1_1_solver3_d_1_1_properties_1_1_resources" ]
        ] ],
        [ "Solver", "namespace_structures_1_1_solver3_d_1_1_solver.html", [
          [ "MatrixSupport", "namespace_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support.html", [
            [ "IGenericMatrix", "interface_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_i_generic_matrix.html", "interface_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_i_generic_matrix" ],
            [ "MatrixOperation", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_matrix_operation.html", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_matrix_operation" ],
            [ "SparseGenericMatrix", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_sparse_generic_matrix.html", "class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_sparse_generic_matrix" ]
          ] ],
          [ "FiniteElementSolver3D", "class_structures_1_1_solver3_d_1_1_solver_1_1_finite_element_solver3_d.html", "class_structures_1_1_solver3_d_1_1_solver_1_1_finite_element_solver3_d" ],
          [ "IterativeFiniteElementSolver3D", "class_structures_1_1_solver3_d_1_1_solver_1_1_iterative_finite_element_solver3_d.html", "class_structures_1_1_solver3_d_1_1_solver_1_1_iterative_finite_element_solver3_d" ]
        ] ],
        [ "GlobalSolverParameters", "class_structures_1_1_solver3_d_1_1_global_solver_parameters.html", "class_structures_1_1_solver3_d_1_1_global_solver_parameters" ]
      ] ],
      [ "SolverDesignModels", "namespace_structures_1_1_solver_design_models.html", [
        [ "DesignElements", "namespace_structures_1_1_solver_design_models_1_1_design_elements.html", [
          [ "AreaElements", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements.html", [
            [ "Profiles", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles.html", [
              [ "MetalDeckProfile", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles_1_1_metal_deck_profile.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_profiles_1_1_metal_deck_profile" ]
            ] ],
            [ "AutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_auto_mesh_options.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_auto_mesh_options" ],
            [ "DesignAreaElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_element" ],
            [ "DesignAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_load" ],
            [ "DesignAreaSpring", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring" ],
            [ "DesignAreaSpringProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring_properties.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_spring_properties" ],
            [ "DesignAreaSupport", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_support.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_support" ],
            [ "DesignCLTElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_c_l_t_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_c_l_t_element" ],
            [ "DesignLinearlyVaryingAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_area_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_area_load" ],
            [ "DesignLinearlyVaryingPercentAreaLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_percent_area_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_linearly_varying_percent_area_load" ],
            [ "DesignOffsetAreaElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_offset_area_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_offset_area_element" ],
            [ "LineSegment2DWrapper", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_line_segment2_d_wrapper.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_line_segment2_d_wrapper" ]
          ] ],
          [ "Extra", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html", [
            [ "ColdFormedResources", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_cold_formed_resources.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_cold_formed_resources" ],
            [ "DesignObject", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_object.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_object" ],
            [ "DesignResults", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results" ],
            [ "DiaphragmPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel" ],
            [ "MechanicalDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mechanical_d_c_r.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mechanical_d_c_r" ],
            [ "MemberDemands", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_member_demands.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_member_demands" ],
            [ "Mils", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils" ],
            [ "Screw", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_screw.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_screw" ],
            [ "ShearWallPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel" ],
            [ "SheathingMaterial", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_material.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_material" ],
            [ "SheathingPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_panel" ],
            [ "SheathingThickness", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_thickness.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_thickness" ],
            [ "StudShape", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape" ]
          ] ],
          [ "LinearElements", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements.html", [
            [ "Connections", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connections.html", [
              [ "ColdFormedConnections", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connecb99839e1181ab88fffe3c19a84a30681.html", [
                [ "FlangeToFlangeConnection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connection622a412699a42db29e6f6e210a75ae94.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connection622a412699a42db29e6f6e210a75ae94" ],
                [ "IColdFormedConnection", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connec967689511b25c885883bc36b25c9d573.html", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connec967689511b25c885883bc36b25c9d573" ]
              ] ],
              [ "IFrameConnection", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connections_1_1_i_frame_connection.html", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_connections_1_1_i_frame_connection" ]
            ] ],
            [ "CrossSections", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections.html", [
              [ "ColdFormedSteel", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_cold_formed_steel.html", [
                [ "Constraints", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectae45e40362383fba770f534e67206fcf.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectae45e40362383fba770f534e67206fcf" ],
                [ "ConstraintSpring", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcdd419d06a3b5df2fefdfedc6f508cea.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcdd419d06a3b5df2fefdfedc6f508cea" ],
                [ "DesignColdFormedBackToBackCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect30a7837db523120846bbeed1e59667b6.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect30a7837db523120846bbeed1e59667b6" ],
                [ "DesignColdFormedBoxedBackToBackCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect28e871ae269a66f14fe91309405f1797.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect28e871ae269a66f14fe91309405f1797" ],
                [ "DesignColdFormedCShapeSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect17ee1eee13d1f284a1ef60f4af1be67e.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect17ee1eee13d1f284a1ef60f4af1be67e" ],
                [ "DesignColdFormedSteelSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fd4dec1ca8e57adcfe96b7cfadb0a.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fd4dec1ca8e57adcfe96b7cfadb0a" ],
                [ "Dimensions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fe4eeeb14610cac8744faac81f948.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fe4eeeb14610cac8744faac81f948" ],
                [ "DistortionalBucklingProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secta29fa85140e4ad9687aa0caa9bc28374.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secta29fa85140e4ad9687aa0caa9bc28374" ],
                [ "HoleProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte9ce7ed942f684b33429e3ca566476ef.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte9ce7ed942f684b33429e3ca566476ef" ],
                [ "ShapeMinMax", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect275d3bc18f27eb063e830427db2af41a.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect275d3bc18f27eb063e830427db2af41a" ],
                [ "ShearStiffenerProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect98b7b25066dd9cee2ae136378b631f2c.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect98b7b25066dd9cee2ae136378b631f2c" ]
              ] ],
              [ "Wood", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_wood.html", [
                [ "DesignGlulamCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf7cefac20e8da9dfe3b91dbf30897038.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf7cefac20e8da9dfe3b91dbf30897038" ],
                [ "DesignSawnLumberCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5eb2e6e83dc58e2a8f1d8ec63e344097.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5eb2e6e83dc58e2a8f1d8ec63e344097" ],
                [ "EffectiveCharDepth", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc3e247cfa5c615cce608bbce952fa11b.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc3e247cfa5c615cce608bbce952fa11b" ]
              ] ],
              [ "ConcreteSectionShearDepth", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectaa3691aad016020bea4a6d149f18d860.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectaa3691aad016020bea4a6d149f18d860" ],
              [ "CrossSectionAssemblyItem", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5f174aab34cce1a2149831b743178020.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5f174aab34cce1a2149831b743178020" ],
              [ "CustomSectionGeometry", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9bc863e4dbc454e81746b778ea5d441d.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9bc863e4dbc454e81746b778ea5d441d" ],
              [ "DesignAssembledCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte76ff215972dbccf3d987113e8562856.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_secte76ff215972dbccf3d987113e8562856" ],
              [ "DesignBendingProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectb52f30af52a56ca03fa466760f9bd2c3.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectb52f30af52a56ca03fa466760f9bd2c3" ],
              [ "DesignChannelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcda38547983eb661565302548f4c504f.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcda38547983eb661565302548f4c504f" ],
              [ "DesignCircularCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectd857442166a589c4dee5ef3d70648e41.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectd857442166a589c4dee5ef3d70648e41" ],
              [ "DesignCoreBraceBRBSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5213eef38b8d6692354c5c2c68ac2fb0.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect5213eef38b8d6692354c5c2c68ac2fb0" ],
              [ "DesignCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_design_cross_section.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_design_cross_section" ],
              [ "DesignCustomConcreteSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9edb935761261f31aebfbc0fd277797b.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9edb935761261f31aebfbc0fd277797b" ],
              [ "DesignCustomHomogeneousSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect6e92485a05232ea2dafe700adfecbcde.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect6e92485a05232ea2dafe700adfecbcde" ],
              [ "DesignMiscChannelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf6293eb321d242a02b281208cc305d9a.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectf6293eb321d242a02b281208cc305d9a" ],
              [ "DesignParallelFlangeChannel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect034839b75bde3fdd88aea3eb06b0df06.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect034839b75bde3fdd88aea3eb06b0df06" ],
              [ "DesignRectangularCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect33b00aabffcc22c4ef9eff65e48942b1.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect33b00aabffcc22c4ef9eff65e48942b1" ],
              [ "DesignRectangularHSSCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2200add24e6878d457d37d1cf03f9383.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2200add24e6878d457d37d1cf03f9383" ],
              [ "DesignRoundHSSCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect450fafc59b666c16e873189322376d9a.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect450fafc59b666c16e873189322376d9a" ],
              [ "DesignSingleAngleCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2456887f1b32d13b065ef4ceb00633a5.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2456887f1b32d13b065ef4ceb00633a5" ],
              [ "DesignSteelCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect1fed346bee88887f93c8487c9adc6176.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect1fed346bee88887f93c8487c9adc6176" ],
              [ "DesignStructuralCompositeLumberSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8ed84c03eb2bb63e164b3b793960e010.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8ed84c03eb2bb63e164b3b793960e010" ],
              [ "DesignTorsionalProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8249da24b5b0f4c13ab908604a1a19a2.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect8249da24b5b0f4c13ab908604a1a19a2" ],
              [ "DesignWideFlangeCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2dd16171a089eff440825a6f6d8f0095.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect2dd16171a089eff440825a6f6d8f0095" ],
              [ "DesignWideFlangeTeeCrossSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect7ef1c6bfd95f15d93d5337668a92d547.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect7ef1c6bfd95f15d93d5337668a92d547" ],
              [ "DesignWoodIJoistSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc358d39d6dde640f7860d59e9489153d.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectc358d39d6dde640f7860d59e9489153d" ],
              [ "SectionProfile", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_section_profile.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sections_1_1_section_profile" ]
            ] ],
            [ "BracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options" ],
            [ "CompositeSteelBeamProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_composite_steel_beam_properties.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_composite_steel_beam_properties" ],
            [ "DesignFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_element" ],
            [ "DesignFrameLineLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_line_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_line_load" ],
            [ "DesignFramePercentWeightLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_percent_weight_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_percent_weight_load" ],
            [ "DesignFramePointLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_point_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_point_load" ],
            [ "DesignFrameThermalLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_thermal_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_thermal_load" ],
            [ "DesignGlobalLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_global_link_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_global_link_element" ],
            [ "DesignLineSupport", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_line_support.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_line_support" ],
            [ "DesignLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_link_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_link_element" ],
            [ "DesignLocalLinkElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_local_link_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_local_link_element" ],
            [ "DesignOffsetFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_offset_frame_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_offset_frame_element" ],
            [ "DesignSteelCompositeFrameElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_steel_composite_frame_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_steel_composite_frame_element" ],
            [ "FrameAutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options" ],
            [ "FrameBracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_bracing_options.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_bracing_options" ],
            [ "FrameDisplacementDiagram", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_displacement_diagram.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_displacement_diagram" ],
            [ "FrameInternalForceOrdinate", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_internal_force_ordinate.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_internal_force_ordinate" ],
            [ "IDesignFrameElement", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_design_frame_element.html", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_design_frame_element" ],
            [ "IOffsetFrameElement", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_offset_frame_element.html", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_i_offset_frame_element" ],
            [ "ReducedBeamSection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_reduced_beam_section.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_reduced_beam_section" ],
            [ "RigidEndZone", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_rigid_end_zone.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_rigid_end_zone" ]
          ] ],
          [ "PointElements", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements.html", [
            [ "DesignNodalDisplacement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_displacement.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_displacement" ],
            [ "DesignNodalForce", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_force.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_nodal_force" ],
            [ "DesignPointElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_point_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_point_element" ],
            [ "DesignRestraintProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_restraint_properties.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_restraint_properties" ],
            [ "DesignSpringProperties", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_spring_properties.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_design_spring_properties" ],
            [ "DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group" ],
            [ "DriftStack", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_stack.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_stack" ],
            [ "IDesignSpringProperties", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_i_design_spring_properties.html", "interface_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_i_design_spring_properties" ],
            [ "NodalDrift", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_nodal_drift.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_nodal_drift" ]
          ] ],
          [ "SolidElements", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements.html", [
            [ "DesignHexahedronElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_hexahedron_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_hexahedron_element" ],
            [ "DesignPentahedronElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_pentahedron_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_pentahedron_element" ],
            [ "DesignSolidElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_element" ],
            [ "DesignSolidExtrusion", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion" ],
            [ "DesignSolidLoad", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_load.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_load" ]
          ] ],
          [ "DesignDelayedAreaRestraint", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_area_restraint.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_area_restraint" ],
          [ "DesignDelayedRestraint", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_restraint.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_delayed_restraint" ],
          [ "DesignElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_element.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_element" ],
          [ "DesignGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_group.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_group" ],
          [ "ElementGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group" ]
        ] ],
        [ "Extensions", "namespace_structures_1_1_solver_design_models_1_1_extensions.html", [
          [ "CurveExtensions", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_curve_extensions.html", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_curve_extensions" ],
          [ "Point3DExtensions", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_point3_d_extensions.html", "class_structures_1_1_solver_design_models_1_1_extensions_1_1_point3_d_extensions" ]
        ] ],
        [ "Materials", "namespace_structures_1_1_solver_design_models_1_1_materials.html", [
          [ "AluminumMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_aluminum_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_aluminum_material" ],
          [ "DesignIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_isotropic_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_isotropic_material" ],
          [ "DesignOrthotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_orthotropic_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_design_orthotropic_material" ],
          [ "GeneralIsotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_isotropic_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_isotropic_material" ],
          [ "GeneralOrthotropicMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_orthotropic_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_general_orthotropic_material" ],
          [ "IDesignMaterial", "interface_structures_1_1_solver_design_models_1_1_materials_1_1_i_design_material.html", "interface_structures_1_1_solver_design_models_1_1_materials_1_1_i_design_material" ],
          [ "MasonryMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_masonry_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_masonry_material" ],
          [ "PrestressingSteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_prestressing_steel_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_prestressing_steel_material" ],
          [ "SteelMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_steel_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_steel_material" ],
          [ "UnconfinedConcreteMaterial", "class_structures_1_1_solver_design_models_1_1_materials_1_1_unconfined_concrete_material.html", "class_structures_1_1_solver_design_models_1_1_materials_1_1_unconfined_concrete_material" ]
        ] ],
        [ "Models", "namespace_structures_1_1_solver_design_models_1_1_models.html", [
          [ "EtabsExporter", "class_structures_1_1_solver_design_models_1_1_models_1_1_etabs_exporter.html", "class_structures_1_1_solver_design_models_1_1_models_1_1_etabs_exporter" ],
          [ "GenericDesignModel", "class_structures_1_1_solver_design_models_1_1_models_1_1_generic_design_model.html", "class_structures_1_1_solver_design_models_1_1_models_1_1_generic_design_model" ],
          [ "GridLine", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_line.html", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_line" ],
          [ "GridSystem", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_system.html", "class_structures_1_1_solver_design_models_1_1_models_1_1_grid_system" ],
          [ "ModelExporter", "class_structures_1_1_solver_design_models_1_1_models_1_1_model_exporter.html", "class_structures_1_1_solver_design_models_1_1_models_1_1_model_exporter" ]
        ] ],
        [ "Settings", "namespace_structures_1_1_solver_design_models_1_1_settings.html", [
          [ "DesignReportSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_design_report_settings" ],
          [ "ModalAnalysisSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings" ],
          [ "ModelDefinitions", "class_structures_1_1_solver_design_models_1_1_settings_1_1_model_definitions.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_model_definitions" ],
          [ "PDeltaSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings" ]
        ] ],
        [ "Steel", "namespace_structures_1_1_solver_design_models_1_1_steel.html", [
          [ "SteelResources", "class_structures_1_1_solver_design_models_1_1_steel_1_1_steel_resources.html", "class_structures_1_1_solver_design_models_1_1_steel_1_1_steel_resources" ],
          [ "WidthToThickness", "class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness.html", "class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness" ]
        ] ],
        [ "Wood", "namespace_structures_1_1_solver_design_models_1_1_wood.html", [
          [ "WoodDesignResources", "class_structures_1_1_solver_design_models_1_1_wood_1_1_wood_design_resources.html", "class_structures_1_1_solver_design_models_1_1_wood_1_1_wood_design_resources" ]
        ] ],
        [ "DesignAreaElementObjectives", "class_structures_1_1_solver_design_models_1_1_design_area_element_objectives.html", "class_structures_1_1_solver_design_models_1_1_design_area_element_objectives" ],
        [ "DesignCrossSectionObjectives", "class_structures_1_1_solver_design_models_1_1_design_cross_section_objectives.html", "class_structures_1_1_solver_design_models_1_1_design_cross_section_objectives" ],
        [ "GlobalModelParameters", "class_structures_1_1_solver_design_models_1_1_global_model_parameters.html", "class_structures_1_1_solver_design_models_1_1_global_model_parameters" ],
        [ "StaticFenix", "class_structures_1_1_solver_design_models_1_1_static_fenix.html", "class_structures_1_1_solver_design_models_1_1_static_fenix" ]
      ] ],
      [ "Utilities", "namespace_structures_1_1_utilities.html", [
        [ "BuildingCodes", "namespace_structures_1_1_utilities_1_1_building_codes.html", [
          [ "BuildingCode", "class_structures_1_1_utilities_1_1_building_codes_1_1_building_code.html", "class_structures_1_1_utilities_1_1_building_codes_1_1_building_code" ],
          [ "SupportedCodes", "class_structures_1_1_utilities_1_1_building_codes_1_1_supported_codes.html", "class_structures_1_1_utilities_1_1_building_codes_1_1_supported_codes" ]
        ] ],
        [ "Charting", "namespace_structures_1_1_utilities_1_1_charting.html", [
          [ "Chart", "class_structures_1_1_utilities_1_1_charting_1_1_chart.html", "class_structures_1_1_utilities_1_1_charting_1_1_chart" ],
          [ "Chart2D", "class_structures_1_1_utilities_1_1_charting_1_1_chart2_d.html", "class_structures_1_1_utilities_1_1_charting_1_1_chart2_d" ]
        ] ],
        [ "Compression", "namespace_structures_1_1_utilities_1_1_compression.html", [
          [ "StringCompression", "class_structures_1_1_utilities_1_1_compression_1_1_string_compression.html", "class_structures_1_1_utilities_1_1_compression_1_1_string_compression" ]
        ] ],
        [ "Containers", "namespace_structures_1_1_utilities_1_1_containers.html", [
          [ "EnhancedObservableCollection", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection.html", "class_structures_1_1_utilities_1_1_containers_1_1_enhanced_observable_collection" ],
          [ "EventListArgs", "class_structures_1_1_utilities_1_1_containers_1_1_event_list_args.html", "class_structures_1_1_utilities_1_1_containers_1_1_event_list_args" ],
          [ "MinMaxBounds", "class_structures_1_1_utilities_1_1_containers_1_1_min_max_bounds.html", "class_structures_1_1_utilities_1_1_containers_1_1_min_max_bounds" ],
          [ "ObservableList", "class_structures_1_1_utilities_1_1_containers_1_1_observable_list.html", "class_structures_1_1_utilities_1_1_containers_1_1_observable_list" ]
        ] ],
        [ "DataStructures", "namespace_structures_1_1_utilities_1_1_data_structures.html", [
          [ "Octree", "namespace_structures_1_1_utilities_1_1_data_structures_1_1_octree.html", [
            [ "BoundingBox", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounding_box.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounding_box" ],
            [ "BoundsOctree", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounds_octree.html", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_bounds_octree" ],
            [ "MathExtensions", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_math_extensions.html", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_math_extensions" ],
            [ "Point", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point" ],
            [ "PointOctree", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point_octree.html", "class_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point_octree" ],
            [ "Ray", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_ray.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_ray" ]
          ] ],
          [ "QuadTree", "namespace_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree.html", [
            [ "Matrix", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_matrix.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_matrix" ],
            [ "PointQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_point_q_t.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_point_q_t" ],
            [ "QuadTree", "class_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_quad_tree.html", "class_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_quad_tree" ],
            [ "Rect", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_rect.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_rect" ],
            [ "SizeQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_size_q_t.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_size_q_t" ],
            [ "VectorQT", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_vector_q_t.html", "struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_vector_q_t" ]
          ] ]
        ] ],
        [ "DotNetNative", "namespace_structures_1_1_utilities_1_1_dot_net_native.html", [
          [ "Extensions", "namespace_structures_1_1_utilities_1_1_dot_net_native_1_1_extensions.html", [
            [ "IEnumerableExtensions", "class_structures_1_1_utilities_1_1_dot_net_native_1_1_extensions_1_1_i_enumerable_extensions.html", "class_structures_1_1_utilities_1_1_dot_net_native_1_1_extensions_1_1_i_enumerable_extensions" ]
          ] ]
        ] ],
        [ "Extensions", "namespace_structures_1_1_utilities_1_1_extensions.html", [
          [ "DotNetNative", "namespace_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native.html", [
            [ "ArrayExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_array_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_array_extensions" ],
            [ "CharExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_char_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_char_extensions" ],
            [ "DoubleExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions" ],
            [ "EnumExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_enum_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_enum_extensions" ],
            [ "ImageExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_image_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_image_extensions" ],
            [ "IntegerExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_integer_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_integer_extensions" ],
            [ "LinkedListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_linked_list_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_linked_list_extensions" ],
            [ "ListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_list_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_list_extensions" ],
            [ "ObjectExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_object_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_object_extensions" ],
            [ "StringExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_string_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_string_extensions" ],
            [ "TypeExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_type_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_type_extensions" ],
            [ "XFile", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_x_file.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_x_file" ]
          ] ],
          [ "TriangleNet", "namespace_structures_1_1_utilities_1_1_extensions_1_1_triangle_net.html", [
            [ "VertexExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_triangle_net_1_1_vertex_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_triangle_net_1_1_vertex_extensions" ]
          ] ]
        ] ],
        [ "Generic", "namespace_structures_1_1_utilities_1_1_generic.html", [
          [ "Enumeration", "class_structures_1_1_utilities_1_1_generic_1_1_enumeration.html", "class_structures_1_1_utilities_1_1_generic_1_1_enumeration" ]
        ] ],
        [ "Geometry", "namespace_structures_1_1_utilities_1_1_geometry.html", [
          [ "Geometry3D", "namespace_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d.html", [
            [ "Point3D", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_point3_d.html", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_point3_d" ],
            [ "Vector3D", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_vector3_d.html", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_vector3_d" ]
          ] ],
          [ "CircleDefinition", "struct_structures_1_1_utilities_1_1_geometry_1_1_circle_definition.html", "struct_structures_1_1_utilities_1_1_geometry_1_1_circle_definition" ],
          [ "Edge", "class_structures_1_1_utilities_1_1_geometry_1_1_edge.html", "class_structures_1_1_utilities_1_1_geometry_1_1_edge" ],
          [ "GenericCircle", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle" ],
          [ "GenericClosedShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_closed_shape" ],
          [ "GenericPoint", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_point.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_point" ],
          [ "GenericPolygon", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polygon.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polygon" ],
          [ "GenericPolyline", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polyline.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_polyline" ],
          [ "GenericShape", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_shape.html", "class_structures_1_1_utilities_1_1_geometry_1_1_generic_shape" ],
          [ "GeometryFunctions", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry_functions.html", "class_structures_1_1_utilities_1_1_geometry_1_1_geometry_functions" ],
          [ "IGenericClosedShape", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_closed_shape.html", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_closed_shape" ],
          [ "IGenericGeometry", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_geometry.html", "interface_structures_1_1_utilities_1_1_geometry_1_1_i_generic_geometry" ],
          [ "PointD", "class_structures_1_1_utilities_1_1_geometry_1_1_point_d.html", "class_structures_1_1_utilities_1_1_geometry_1_1_point_d" ],
          [ "Vector", "class_structures_1_1_utilities_1_1_geometry_1_1_vector.html", "class_structures_1_1_utilities_1_1_geometry_1_1_vector" ]
        ] ],
        [ "Geospatial", "namespace_structures_1_1_utilities_1_1_geospatial.html", [
          [ "GeospatialMethods", "class_structures_1_1_utilities_1_1_geospatial_1_1_geospatial_methods.html", "class_structures_1_1_utilities_1_1_geospatial_1_1_geospatial_methods" ]
        ] ],
        [ "Graphics", "namespace_structures_1_1_utilities_1_1_graphics.html", [
          [ "ColorGradient", "class_structures_1_1_utilities_1_1_graphics_1_1_color_gradient.html", "class_structures_1_1_utilities_1_1_graphics_1_1_color_gradient" ],
          [ "IColorizable", "interface_structures_1_1_utilities_1_1_graphics_1_1_i_colorizable.html", "interface_structures_1_1_utilities_1_1_graphics_1_1_i_colorizable" ]
        ] ],
        [ "HttpRequests", "namespace_structures_1_1_utilities_1_1_http_requests.html", [
          [ "ASCE_7_Hazard_Tool", "class_structures_1_1_utilities_1_1_http_requests_1_1_a_s_c_e__7___hazard___tool.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_a_s_c_e__7___hazard___tool" ],
          [ "Data", "class_structures_1_1_utilities_1_1_http_requests_1_1_data.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_data" ],
          [ "GeneralQueries", "class_structures_1_1_utilities_1_1_http_requests_1_1_general_queries.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_general_queries" ],
          [ "Geolocation", "class_structures_1_1_utilities_1_1_http_requests_1_1_geolocation.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_geolocation" ],
          [ "Metadata", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata" ],
          [ "Parameters", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_parameters" ],
          [ "Request", "class_structures_1_1_utilities_1_1_http_requests_1_1_request.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_request" ],
          [ "Response", "class_structures_1_1_utilities_1_1_http_requests_1_1_response.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_response" ],
          [ "SeismicDesignData", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_design_data.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_design_data" ],
          [ "SeismicQueryUSGS", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_query_u_s_g_s.html", "class_structures_1_1_utilities_1_1_http_requests_1_1_seismic_query_u_s_g_s" ]
        ] ],
        [ "Loads", "namespace_structures_1_1_utilities_1_1_loads.html", [
          [ "Seismic", "namespace_structures_1_1_utilities_1_1_loads_1_1_seismic.html", [
            [ "SeismicDesignResources", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_design_resources.html", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_design_resources" ],
            [ "SeismicForceResistingSystem", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_force_resisting_system.html", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_seismic_force_resisting_system" ],
            [ "StructuralHeightLimit", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_height_limit.html", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_height_limit" ],
            [ "StructuralSystemLimitations", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_system_limitations.html", "class_structures_1_1_utilities_1_1_loads_1_1_seismic_1_1_structural_system_limitations" ]
          ] ],
          [ "Snow", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow.html", [
            [ "ASCE_7_16", "namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html", [
              [ "SnowLoadingParameters", "class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters.html", "class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters" ]
            ] ]
          ] ],
          [ "Wind", "namespace_structures_1_1_utilities_1_1_loads_1_1_wind.html", [
            [ "ParapetPressure", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_parapet_pressure.html", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_parapet_pressure" ],
            [ "WallPressure", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure.html", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure" ],
            [ "WallPressureTable", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure_table.html", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wall_pressure_table" ],
            [ "WindParameters", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wind_parameters.html", "class_structures_1_1_utilities_1_1_loads_1_1_wind_1_1_wind_parameters" ]
          ] ],
          [ "AccidentalEccentricySettings", "class_structures_1_1_utilities_1_1_loads_1_1_accidental_eccentricy_settings.html", "class_structures_1_1_utilities_1_1_loads_1_1_accidental_eccentricy_settings" ],
          [ "ASCE41_17_EquivalentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e41__17___equivalent_lateral_force.html", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e41__17___equivalent_lateral_force" ],
          [ "ASCE7_16_EquivalentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___equivalent_lateral_force.html", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___equivalent_lateral_force" ],
          [ "ASCE7_16_UserCs", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___user_cs.html", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e7__16___user_cs" ],
          [ "ASCE_EquivlaentLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e___equivlaent_lateral_force.html", "class_structures_1_1_utilities_1_1_loads_1_1_a_s_c_e___equivlaent_lateral_force" ],
          [ "AutoLateralForce", "class_structures_1_1_utilities_1_1_loads_1_1_auto_lateral_force.html", "class_structures_1_1_utilities_1_1_loads_1_1_auto_lateral_force" ],
          [ "LoadCombination", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination.html", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination" ],
          [ "LoadCombinationDeflectionCriteria", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria" ],
          [ "LoadDurationType", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type.html", "class_structures_1_1_utilities_1_1_loads_1_1_load_duration_type" ],
          [ "LoadPattern", "class_structures_1_1_utilities_1_1_loads_1_1_load_pattern.html", "class_structures_1_1_utilities_1_1_loads_1_1_load_pattern" ],
          [ "PatternResponseSpectrumProperties", "class_structures_1_1_utilities_1_1_loads_1_1_pattern_response_spectrum_properties.html", "class_structures_1_1_utilities_1_1_loads_1_1_pattern_response_spectrum_properties" ],
          [ "ResponseSpectrum", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum.html", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum" ],
          [ "ResponseSpectrumOrdinate", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum_ordinate.html", "class_structures_1_1_utilities_1_1_loads_1_1_response_spectrum_ordinate" ]
        ] ],
        [ "Logging", "namespace_structures_1_1_utilities_1_1_logging.html", [
          [ "FenixLogger", "class_structures_1_1_utilities_1_1_logging_1_1_fenix_logger.html", "class_structures_1_1_utilities_1_1_logging_1_1_fenix_logger" ],
          [ "LogSolver3D", "class_structures_1_1_utilities_1_1_logging_1_1_log_solver3_d.html", "class_structures_1_1_utilities_1_1_logging_1_1_log_solver3_d" ]
        ] ],
        [ "MathFunctions", "namespace_structures_1_1_utilities_1_1_math_functions.html", [
          [ "CommonMathFunctions", "class_structures_1_1_utilities_1_1_math_functions_1_1_common_math_functions.html", "class_structures_1_1_utilities_1_1_math_functions_1_1_common_math_functions" ]
        ] ],
        [ "MatrixLibrary", "namespace_structures_1_1_utilities_1_1_matrix_library.html", [
          [ "MatrixLibrary", "class_structures_1_1_utilities_1_1_matrix_library_1_1_matrix_library.html", "class_structures_1_1_utilities_1_1_matrix_library_1_1_matrix_library" ]
        ] ],
        [ "Modal", "namespace_structures_1_1_utilities_1_1_modal.html", [
          [ "MassFromLoadPattern", "class_structures_1_1_utilities_1_1_modal_1_1_mass_from_load_pattern.html", "class_structures_1_1_utilities_1_1_modal_1_1_mass_from_load_pattern" ],
          [ "MassSource", "class_structures_1_1_utilities_1_1_modal_1_1_mass_source.html", "class_structures_1_1_utilities_1_1_modal_1_1_mass_source" ]
        ] ],
        [ "Observable", "namespace_structures_1_1_utilities_1_1_observable.html", [
          [ "ObservableString", "class_structures_1_1_utilities_1_1_observable_1_1_observable_string.html", "class_structures_1_1_utilities_1_1_observable_1_1_observable_string" ]
        ] ],
        [ "PolyBool", "namespace_structures_1_1_utilities_1_1_poly_bool.html", [
          [ "PolyBoolExtensions", "class_structures_1_1_utilities_1_1_poly_bool_1_1_poly_bool_extensions.html", "class_structures_1_1_utilities_1_1_poly_bool_1_1_poly_bool_extensions" ]
        ] ],
        [ "PrimitiveGeometry", "namespace_structures_1_1_utilities_1_1_primitive_geometry.html", [
          [ "Extensions", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions.html", [
            [ "LineSegment2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment2_d_extensions" ],
            [ "LineSegment3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions" ],
            [ "PlaneExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_plane_extensions" ],
            [ "Point2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point2_d_extensions" ],
            [ "Point3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_point3_d_extensions" ],
            [ "Polygon2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions" ],
            [ "Polygon3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon3_d_extensions" ],
            [ "Polyline2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polyline2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polyline2_d_extensions" ],
            [ "Vector2DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector2_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector2_d_extensions" ],
            [ "Vector3DExtensions", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector3_d_extensions.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_vector3_d_extensions" ]
          ] ],
          [ "Helpers", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers.html", [
            [ "SimplePolygonException", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers_1_1_simple_polygon_exception.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers_1_1_simple_polygon_exception" ]
          ] ],
          [ "GeometryHelper", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_geometry_helper.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_geometry_helper" ],
          [ "InclinedRotations", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations" ],
          [ "Point3DList", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_point3_d_list.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_point3_d_list" ],
          [ "Polygon3D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_polygon3_d.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_polygon3_d" ],
          [ "SimplePolygon2D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_simple_polygon2_d.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_simple_polygon2_d" ]
        ] ],
        [ "Reporting", "namespace_structures_1_1_utilities_1_1_reporting.html", [
          [ "Equations", "namespace_structures_1_1_utilities_1_1_reporting_1_1_equations.html", [
            [ "CalculationLog", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_calculation_log.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_calculation_log" ],
            [ "DesignCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_design_calculation.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_design_calculation" ],
            [ "EquationDisplayParameters", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_equation_display_parameters.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_equation_display_parameters" ],
            [ "ExpressionlessDesignCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_expressionless_design_calculation.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_expressionless_design_calculation" ],
            [ "LaTexVariable", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_la_tex_variable.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_la_tex_variable" ],
            [ "RecordedCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_recorded_calculation.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_recorded_calculation" ],
            [ "ResultLimit", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_result_limit.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_result_limit" ],
            [ "SubCalculation", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_sub_calculation.html", "class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_sub_calculation" ]
          ] ],
          [ "Tabular", "namespace_structures_1_1_utilities_1_1_reporting_1_1_tabular.html", [
            [ "ColumnDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_column_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_column_display_settings" ],
            [ "DataTableExtensions", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_data_table_extensions.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_data_table_extensions" ],
            [ "NumericDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_numeric_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_numeric_display_settings" ],
            [ "Row", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_row.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_row" ],
            [ "SortOrder", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order" ],
            [ "StringDisplaySettings", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_string_display_settings.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_string_display_settings" ],
            [ "Table", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table" ]
          ] ],
          [ "Html", "class_structures_1_1_utilities_1_1_reporting_1_1_html.html", "class_structures_1_1_utilities_1_1_reporting_1_1_html" ],
          [ "Printer", "class_structures_1_1_utilities_1_1_reporting_1_1_printer.html", "class_structures_1_1_utilities_1_1_reporting_1_1_printer" ],
          [ "ProjectInformation", "class_structures_1_1_utilities_1_1_reporting_1_1_project_information.html", "class_structures_1_1_utilities_1_1_reporting_1_1_project_information" ],
          [ "ReferenceStandard", "class_structures_1_1_utilities_1_1_reporting_1_1_reference_standard.html", "class_structures_1_1_utilities_1_1_reporting_1_1_reference_standard" ]
        ] ],
        [ "Serialization", "namespace_structures_1_1_utilities_1_1_serialization.html", [
          [ "SerializeSettings", "class_structures_1_1_utilities_1_1_serialization_1_1_serialize_settings.html", "class_structures_1_1_utilities_1_1_serialization_1_1_serialize_settings" ]
        ] ],
        [ "SolverSupport", "namespace_structures_1_1_utilities_1_1_solver_support.html", [
          [ "Diaphragm", "class_structures_1_1_utilities_1_1_solver_support_1_1_diaphragm.html", "class_structures_1_1_utilities_1_1_solver_support_1_1_diaphragm" ]
        ] ],
        [ "Svg", "namespace_structures_1_1_utilities_1_1_svg.html", [
          [ "DrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_drawing_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_drawing_properties" ],
          [ "SvgDrawUtility", "class_structures_1_1_utilities_1_1_svg_1_1_svg_draw_utility.html", "class_structures_1_1_utilities_1_1_svg_1_1_svg_draw_utility" ],
          [ "SVGProperties", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_s_v_g_properties" ],
          [ "TooltipText", "class_structures_1_1_utilities_1_1_svg_1_1_tooltip_text.html", "class_structures_1_1_utilities_1_1_svg_1_1_tooltip_text" ],
          [ "TriangleDrawingProperties", "class_structures_1_1_utilities_1_1_svg_1_1_triangle_drawing_properties.html", "class_structures_1_1_utilities_1_1_svg_1_1_triangle_drawing_properties" ]
        ] ],
        [ "Threading", "namespace_structures_1_1_utilities_1_1_threading.html", [
          [ "LimitedConcurrencyLevelTaskScheduler", "class_structures_1_1_utilities_1_1_threading_1_1_limited_concurrency_level_task_scheduler.html", "class_structures_1_1_utilities_1_1_threading_1_1_limited_concurrency_level_task_scheduler" ],
          [ "ProcessorCores", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores.html", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores" ],
          [ "TaskExtensions", "class_structures_1_1_utilities_1_1_threading_1_1_task_extensions.html", "class_structures_1_1_utilities_1_1_threading_1_1_task_extensions" ],
          [ "ThreadExtension", "class_structures_1_1_utilities_1_1_threading_1_1_thread_extension.html", "class_structures_1_1_utilities_1_1_threading_1_1_thread_extension" ],
          [ "ThreadSafeList", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list.html", "class_structures_1_1_utilities_1_1_threading_1_1_thread_safe_list" ]
        ] ],
        [ "TriangleNetHelper", "namespace_structures_1_1_utilities_1_1_triangle_net_helper.html", [
          [ "Geometry", "namespace_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry.html", [
            [ "IShape", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape.html", "interface_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_i_shape" ],
            [ "PreEdge", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_edge.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_edge" ],
            [ "PrePolygon", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polygon.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polygon" ],
            [ "PrePolyline", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polyline.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polyline" ],
            [ "PreVertex", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_vertex.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_vertex" ]
          ] ],
          [ "GeometryFunctions", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_functions.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_functions" ],
          [ "MeshingOptions", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_meshing_options.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_meshing_options" ],
          [ "PreProcessor", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor" ],
          [ "PreProcessorQT", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor_q_t.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_pre_processor_q_t" ],
          [ "TriangleHelper", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_helper.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_helper" ],
          [ "TriangleNetInvalidPolygonException", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_net_invalid_polygon_exception.html", "class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_triangle_net_invalid_polygon_exception" ]
        ] ],
        [ "Units", "namespace_structures_1_1_utilities_1_1_units.html", [
          [ "Conversion", "namespace_structures_1_1_utilities_1_1_units_1_1_conversion.html", [
            [ "Acceleration", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_acceleration.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_acceleration" ],
            [ "Area", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_area.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_area" ],
            [ "Constants", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_constants.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_constants" ],
            [ "Density", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_density.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_density" ],
            [ "FlexuralStiffness", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_flexural_stiffness.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_flexural_stiffness" ],
            [ "FLT", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_f_l_t.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_f_l_t" ],
            [ "Force", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force" ],
            [ "ForcePerLength", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force_per_length.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_force_per_length" ],
            [ "Length", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length" ],
            [ "LengthCubed", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_cubed.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_cubed" ],
            [ "LengthToThe4th", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the4th.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the4th" ],
            [ "LengthToThe6th", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the6th.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_length_to_the6th" ],
            [ "Moment", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_moment.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_moment" ],
            [ "Result", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_result.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_result" ],
            [ "Stress", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_stress.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_stress" ],
            [ "Time", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_time.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_time" ],
            [ "Undefined", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_undefined.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_undefined" ],
            [ "Unitless", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_unitless.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_unitless" ],
            [ "Utilities", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_utilities.html", "class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_utilities" ]
          ] ],
          [ "Parsing", "namespace_structures_1_1_utilities_1_1_units_1_1_parsing.html", [
            [ "Parse", "class_structures_1_1_utilities_1_1_units_1_1_parsing_1_1_parse.html", "class_structures_1_1_utilities_1_1_units_1_1_parsing_1_1_parse" ]
          ] ],
          [ "AccelerationUnit", "class_structures_1_1_utilities_1_1_units_1_1_acceleration_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_acceleration_unit" ],
          [ "AreaUnit", "class_structures_1_1_utilities_1_1_units_1_1_area_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_area_unit" ],
          [ "DensityUnit", "class_structures_1_1_utilities_1_1_units_1_1_density_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_density_unit" ],
          [ "FlexuralStiffnessUnit", "class_structures_1_1_utilities_1_1_units_1_1_flexural_stiffness_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_flexural_stiffness_unit" ],
          [ "ForcePerLengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_force_per_length_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_force_per_length_unit" ],
          [ "ForceUnit", "class_structures_1_1_utilities_1_1_units_1_1_force_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_force_unit" ],
          [ "Gauge", "class_structures_1_1_utilities_1_1_units_1_1_gauge.html", "class_structures_1_1_utilities_1_1_units_1_1_gauge" ],
          [ "LengthCubedUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_cubed_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_length_cubed_unit" ],
          [ "LengthToThe4thUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the4th_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the4th_unit" ],
          [ "LengthToThe6thUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the6th_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_length_to_the6th_unit" ],
          [ "LengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_length_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_length_unit" ],
          [ "MomentPerLengthUnit", "class_structures_1_1_utilities_1_1_units_1_1_moment_per_length_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_moment_per_length_unit" ],
          [ "MomentUnit", "class_structures_1_1_utilities_1_1_units_1_1_moment_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_moment_unit" ],
          [ "StressUnit", "class_structures_1_1_utilities_1_1_units_1_1_stress_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_stress_unit" ],
          [ "TimeUnit", "class_structures_1_1_utilities_1_1_units_1_1_time_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_time_unit" ],
          [ "UndefinedUnit", "class_structures_1_1_utilities_1_1_units_1_1_undefined_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_undefined_unit" ],
          [ "Unit", "class_structures_1_1_utilities_1_1_units_1_1_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_unit" ],
          [ "UnitlessUnit", "class_structures_1_1_utilities_1_1_units_1_1_unitless_unit.html", "class_structures_1_1_utilities_1_1_units_1_1_unitless_unit" ],
          [ "UnitSystem", "class_structures_1_1_utilities_1_1_units_1_1_unit_system.html", "class_structures_1_1_utilities_1_1_units_1_1_unit_system" ]
        ] ]
      ] ]
    ] ]
];