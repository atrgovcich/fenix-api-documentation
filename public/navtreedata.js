/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Fenix", "index.html", [
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"class_structures_1_1_materials_1_1_elastic_1_1_wood_1_1_laminated_strand_lumber_design_values.html#aefe4d066184bd0eab77e30f4930cab3b",
"class_structures_1_1_materials_1_1_inelastic_1_1_concrete_1_1_confinement_properties.html#afff59413c0eefed80d01233d15aa9cec",
"class_structures_1_1_materials_1_1_inelastic_1_1_steel_1_1_mander_steel.html#aa1a66d952954fffd76d0c3da19db598b",
"class_structures_1_1_math_net_custom_1_1_spatial_1_1_internals_1_1_convex_hull.html#a468e9698abdf0c9961a03ad8ef101b84",
"class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_frame_element.html#ac8c8df667376f12f174962cbed7c97d1",
"class_structures_1_1_solver3_d_1_1_elements_1_1_linear_1_1_link_element_end_force.html#a2db975cebff03c859979cf5e0bacb3a0",
"class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_bending_tensor.html#afe92da2cc30f4cb99ebccf9e28135943",
"class_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_shell_stress_tensor.html#a48f0ac176130fefcb5a4689631bb7709",
"class_structures_1_1_solver3_d_1_1_elements_1_1_point_1_1_one_node3_d_spring_element.html#a74c59b75cbf271191634a9172b500015",
"class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_iso_p8_node_hexahedron.html#a591d87488197c7c9975e39c1c6256a33",
"class_structures_1_1_solver3_d_1_1_elements_1_1_solid_1_1_solid_stress_tensor.html#a8b515453e297c1e1d51a69f5262569fc",
"class_structures_1_1_solver3_d_1_1_general_1_1_solver_analysis_options.html#ad0cf738dc67f6a08cdf09379b2f15377",
"class_structures_1_1_solver3_d_1_1_loads_1_1_imposed_nodal_displacement.html#afb92570c44df0fcd0cd4fd64cb8dd35e",
"class_structures_1_1_solver3_d_1_1_nodes_1_1_nodal_displacement.html#a9d6846786b949d45edc161c452feedb9",
"class_structures_1_1_solver3_d_1_1_solver_1_1_matrix_support_1_1_sparse_generic_matrix.html#a5b9d777693b3288e88419aaee59c7d40",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_area_elements_1_1_design_area_load.html#a6e64bd6c0b35f1d034e81589145ff582",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_design_element.html#a11e8834ae7c2972ac7a6da00aefc19bc",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils.html#ad8c1bda39601d823550f04c3c0dbd181",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect1fed346bee88887f93c8487c9adc6176.html#a33ce3c8f3af851fc8ea80f5370ad4144",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect450fafc59b666c16e873189322376d9a.html",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sect9bc863e4dbc454e81746b778ea5d441d.html#a9765c097f83f3162bd5c78a8e4eabc84",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectcda38547983eb661565302548f4c504f.html#a683aa838cb7f5734082827ac05533f19",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_cross_sectfc4fe4eeeb14610cac8744faac81f948.html#a9770e9fb959e88862028ef9f4561f88b",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_design_frame_element.html#af91bccff7d42da709587d771c65f1a9e",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_displacement_diagram.html#a785ea78ad4fe51f5a8243eef9ff772ad",
"class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a0c6998c6d791db94427ecdc2fbcc64e4",
"class_structures_1_1_solver_design_models_1_1_materials_1_1_aluminum_material.html#a33ac3014d8981498cd1d4994c0a8f4cf",
"class_structures_1_1_solver_design_models_1_1_models_1_1_etabs_exporter.html#ac9af72315d6242e784a3b76e1504b28a",
"class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness.html#a614b301db756a39392a9d2fc278fe1ef",
"class_structures_1_1_utilities_1_1_containers_1_1_observable_list.html#aefe33f494f3b63f086407973d50ba531",
"class_structures_1_1_utilities_1_1_geometry_1_1_generic_circle.html#a2c04e7638234b36f2530f70af50c336f",
"class_structures_1_1_utilities_1_1_geometry_1_1_geometry3_d_1_1_vector3_d.html",
"class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a0bf99849202c33d8c270aff722ada029",
"class_structures_1_1_utilities_1_1_loads_1_1_load_combination.html#a81f6a1bda40f123898e36a7ebc5a349e",
"class_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16_1_1_snow_loading_parameters.html#ac0d63549087fcd1e765dfdba446fb77d",
"class_structures_1_1_utilities_1_1_matrix_library_1_1_matrix_library.html#abe1bdbe04ad3061112823eed1f54a415",
"class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_polygon2_d_extensions.html#a1a9d9db55f1a9fab96a57cebf7e3bbd5",
"class_structures_1_1_utilities_1_1_reporting_1_1_equations_1_1_design_calculation.html#abf10ad58011badd15d9c2f0e609b898e",
"class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_sort_order.html",
"class_structures_1_1_utilities_1_1_triangle_net_helper_1_1_geometry_1_1_pre_polyline.html",
"class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_constants.html#a0e453b5fc165109ea4341dbcedbaa480",
"class_structures_1_1_utilities_1_1_units_1_1_conversion_1_1_result.html#a4ba4eecd4cadcf7b38afa60d3682f1e8",
"class_structures_1_1_utilities_1_1_units_1_1_gauge.html#ae496885238ba33988d4a48032891c6e7",
"functions_func_u.html",
"interface_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_i_bending_formulation.html#a78f8a58011860a769ce13a62c2c36488",
"namespace_structures_1_1_solver3_d_1_1_elements_1_1_planar_1_1_quadrilateral.html",
"namespace_structures_1_1_utilities_1_1_charting.html#a86326fd1a5a6b6e4d2912590bd0dabe6a9da618abf48879b8301c4cb9a974dc05",
"struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_circle3_d.html#a0740dfd99efa47d6db2b844990970859",
"struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_point3_d.html#a3e826818fc3e7262ee69919513b21e5d",
"struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_ray3_d.html#ac4bfab03a96d64b52e0e4877e8bdf47b",
"struct_structures_1_1_math_net_custom_1_1_spatial_1_1_euclidean_1_1_vector3_d.html#aba90ed12da9c9f7d16dbc790b345b022",
"struct_structures_1_1_utilities_1_1_data_structures_1_1_octree_1_1_point.html#ac473e2939e17f74022d91046a0210677",
"struct_structures_1_1_utilities_1_1_data_structures_1_1_quad_tree_1_1_vector_q_t.html#a07d101b08a713611a458c8e53f95bad9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';