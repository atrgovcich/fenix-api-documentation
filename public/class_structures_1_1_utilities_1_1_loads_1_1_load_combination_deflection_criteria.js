var class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria =
[
    [ "Copy", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html#a55912b36005919907851521d615d9cf2", null ],
    [ "BeamDeflectionRatio", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html#aee935d93c933fb4843bf64c42bc9257d", null ],
    [ "ColumnDriftRatio", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html#abedf28a91c58266e01a4105d7190c324", null ],
    [ "EvaluateBeams", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html#a8012d09c6cdee436baf30a63cb869673", null ],
    [ "EvaluateColumns", "class_structures_1_1_utilities_1_1_loads_1_1_load_combination_deflection_criteria.html#ad969bc22ebe90e2b7fc6e19f3345c33e", null ]
];