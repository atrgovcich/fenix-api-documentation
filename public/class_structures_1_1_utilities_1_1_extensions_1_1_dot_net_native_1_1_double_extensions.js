var class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions =
[
    [ "AbsoluteValueEnvelope", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#a528beb4cd32e68d18a54f70fd03020e0", null ],
    [ "ContainsNanOrInfinity", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#a8396693b94ac1cadd2fbc5078ce60566", null ],
    [ "IsBasicallyAnInteger", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#abe1dc923b1d4d00c85556c0457b664a2", null ],
    [ "IsBasicallyAnInteger", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#aa937d19298f6ffaef9a43dc6205a6004", null ],
    [ "IsNanOrInfinity", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#aca6e73301743cba8c6d8430e4c38c291", null ],
    [ "Pow", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#a05e24a723bd2ae8c5180145a4ed51f57", null ],
    [ "RoundWithSmallValueSupport", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#ad4779746fa9079454a304d37bf842287", null ],
    [ "SubMatrix", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#acb6c7bdfba7a6308ea518812bed9d2f5", null ],
    [ "ToDegrees", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#a0076046edfb38cf5d5aeef430bfc1a28", null ],
    [ "ToRadians", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#add90763fe375741a47c7a954b9a4e54f", null ],
    [ "ToStringWithTrailingZeros", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html#ae3563d391f9f4f914a2aa820ec0a72d4", null ]
];