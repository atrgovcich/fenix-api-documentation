var namespace_structures_1_1_utilities_1_1_geospatial =
[
    [ "GeospatialMethods", "class_structures_1_1_utilities_1_1_geospatial_1_1_geospatial_methods.html", "class_structures_1_1_utilities_1_1_geospatial_1_1_geospatial_methods" ],
    [ "EpsgDesignation", "namespace_structures_1_1_utilities_1_1_geospatial.html#aedc2d2fcd5f510fb3d8300c1c314a086", [
      [ "WGS84", "namespace_structures_1_1_utilities_1_1_geospatial.html#aedc2d2fcd5f510fb3d8300c1c314a086a5dda43a21474cf33e7088b8247f19c4b", null ],
      [ "NAD83_ConusAlbers", "namespace_structures_1_1_utilities_1_1_geospatial.html#aedc2d2fcd5f510fb3d8300c1c314a086a5f9ddec8b0403cc0cd67476c51b83809", null ]
    ] ]
];