var class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table =
[
    [ "Table", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a017439b9e2b357e0ea63631ca7f4625c", null ],
    [ "AddColumn", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a2349ae8221cd921ed4569194c0caa2d3", null ],
    [ "AddRow", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#af82c770206c974a80603f2970e2538a5", null ],
    [ "DistinctTextFromColumn", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a26b63741b954b48e56544e051a6f59fb", null ],
    [ "NewRow", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a4fd486a4f25ccf0c8d350eb66e804877", null ],
    [ "PrintToMarkdown", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a6ac0eb26b55c5a33a35257a1e0aa5e5b", null ],
    [ "SetColumnAlignment", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#af8fe943d157e5d3be21c965a4bb05248", null ],
    [ "SetColumnNumericDisplayPrecision", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#af069195adc8fa8e68e6dcd7994576b33", null ],
    [ "SetColumnTextCapitalization", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a8397f4beb10a3e2331fdd89d4f7dfbca", null ],
    [ "SetExpression", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a700af1a74742a78aaa339857b393c24c", null ],
    [ "SortByColumn", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a09c6ff56f3cf107e171fb79d342f492b", null ],
    [ "SortByColumns", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a46000c2301011ae06d5747c39cc805a6", null ],
    [ "SortByColumns", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a032bc05375422186d78bb22aad39ecd8", null ],
    [ "SortByColumns", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a584040ead3b539ecc34ec8fe69b217b5", null ],
    [ "TextFromColumn", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a0f569eea5590fa20080da37d9dbdedb8", null ],
    [ "ColumnFormattingOptions", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a57ad6858cd3bbff78fb1ea450147caa4", null ],
    [ "Data", "class_structures_1_1_utilities_1_1_reporting_1_1_tabular_1_1_table.html#a2328ec965f5526653ba4c31b8026cd60", null ]
];