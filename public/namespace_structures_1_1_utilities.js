var namespace_structures_1_1_utilities =
[
    [ "Authentication", "namespace_structures_1_1_utilities_1_1_authentication.html", null ],
    [ "BuildingCodes", "namespace_structures_1_1_utilities_1_1_building_codes.html", "namespace_structures_1_1_utilities_1_1_building_codes" ],
    [ "Cartesian", "namespace_structures_1_1_utilities_1_1_cartesian.html", [
      [ "GlobalDisplacementIdentifier", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228", [
        [ "UX", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228ae078410d1456cdb4bc4cec1d02ba3298", null ],
        [ "UY", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228a9f72f02c2586ff913854a844f10d9a99", null ],
        [ "UZ", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228a195dc214249dab3c31906b4b9f83d503", null ],
        [ "RX", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228af9c24782c24c237d16e79f18e2fa9046", null ],
        [ "RY", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228accb21680cb44cbc3715ed8acc0145efe", null ],
        [ "RZ", "namespace_structures_1_1_utilities_1_1_cartesian.html#ae63ada51bc5b0a089ee22469fa96e228a25e29a048984cda66521f1eab1182666", null ]
      ] ],
      [ "LocalAxisIdentifier", "namespace_structures_1_1_utilities_1_1_cartesian.html#a7a349aee63614adee5ce8d7f14d90ef8", [
        [ "X", "namespace_structures_1_1_utilities_1_1_cartesian.html#a7a349aee63614adee5ce8d7f14d90ef8a02129bb861061d1a052c592e2dc6b383", null ],
        [ "Y", "namespace_structures_1_1_utilities_1_1_cartesian.html#a7a349aee63614adee5ce8d7f14d90ef8a57cec4137b614c87cb4e24a3d003a3e0", null ],
        [ "Z", "namespace_structures_1_1_utilities_1_1_cartesian.html#a7a349aee63614adee5ce8d7f14d90ef8a21c2e59531c8710156d34a3c30ac81d5", null ]
      ] ]
    ] ],
    [ "Charting", "namespace_structures_1_1_utilities_1_1_charting.html", "namespace_structures_1_1_utilities_1_1_charting" ],
    [ "Compression", "namespace_structures_1_1_utilities_1_1_compression.html", "namespace_structures_1_1_utilities_1_1_compression" ],
    [ "Containers", "namespace_structures_1_1_utilities_1_1_containers.html", "namespace_structures_1_1_utilities_1_1_containers" ],
    [ "DataStructures", "namespace_structures_1_1_utilities_1_1_data_structures.html", "namespace_structures_1_1_utilities_1_1_data_structures" ],
    [ "DotNetNative", "namespace_structures_1_1_utilities_1_1_dot_net_native.html", "namespace_structures_1_1_utilities_1_1_dot_net_native" ],
    [ "Extensions", "namespace_structures_1_1_utilities_1_1_extensions.html", "namespace_structures_1_1_utilities_1_1_extensions" ],
    [ "Generic", "namespace_structures_1_1_utilities_1_1_generic.html", "namespace_structures_1_1_utilities_1_1_generic" ],
    [ "Geometry", "namespace_structures_1_1_utilities_1_1_geometry.html", "namespace_structures_1_1_utilities_1_1_geometry" ],
    [ "Geospatial", "namespace_structures_1_1_utilities_1_1_geospatial.html", "namespace_structures_1_1_utilities_1_1_geospatial" ],
    [ "Graphics", "namespace_structures_1_1_utilities_1_1_graphics.html", "namespace_structures_1_1_utilities_1_1_graphics" ],
    [ "HttpRequests", "namespace_structures_1_1_utilities_1_1_http_requests.html", "namespace_structures_1_1_utilities_1_1_http_requests" ],
    [ "Loads", "namespace_structures_1_1_utilities_1_1_loads.html", "namespace_structures_1_1_utilities_1_1_loads" ],
    [ "Logging", "namespace_structures_1_1_utilities_1_1_logging.html", "namespace_structures_1_1_utilities_1_1_logging" ],
    [ "MathFunctions", "namespace_structures_1_1_utilities_1_1_math_functions.html", "namespace_structures_1_1_utilities_1_1_math_functions" ],
    [ "MatrixLibrary", "namespace_structures_1_1_utilities_1_1_matrix_library.html", "namespace_structures_1_1_utilities_1_1_matrix_library" ],
    [ "Modal", "namespace_structures_1_1_utilities_1_1_modal.html", "namespace_structures_1_1_utilities_1_1_modal" ],
    [ "Observable", "namespace_structures_1_1_utilities_1_1_observable.html", "namespace_structures_1_1_utilities_1_1_observable" ],
    [ "PolyBool", "namespace_structures_1_1_utilities_1_1_poly_bool.html", "namespace_structures_1_1_utilities_1_1_poly_bool" ],
    [ "PrimitiveGeometry", "namespace_structures_1_1_utilities_1_1_primitive_geometry.html", "namespace_structures_1_1_utilities_1_1_primitive_geometry" ],
    [ "Properties", "namespace_structures_1_1_utilities_1_1_properties.html", null ],
    [ "Reporting", "namespace_structures_1_1_utilities_1_1_reporting.html", "namespace_structures_1_1_utilities_1_1_reporting" ],
    [ "Serialization", "namespace_structures_1_1_utilities_1_1_serialization.html", "namespace_structures_1_1_utilities_1_1_serialization" ],
    [ "SolverSupport", "namespace_structures_1_1_utilities_1_1_solver_support.html", "namespace_structures_1_1_utilities_1_1_solver_support" ],
    [ "Svg", "namespace_structures_1_1_utilities_1_1_svg.html", "namespace_structures_1_1_utilities_1_1_svg" ],
    [ "Threading", "namespace_structures_1_1_utilities_1_1_threading.html", "namespace_structures_1_1_utilities_1_1_threading" ],
    [ "TriangleNetHelper", "namespace_structures_1_1_utilities_1_1_triangle_net_helper.html", "namespace_structures_1_1_utilities_1_1_triangle_net_helper" ],
    [ "Units", "namespace_structures_1_1_utilities_1_1_units.html", "namespace_structures_1_1_utilities_1_1_units" ]
];