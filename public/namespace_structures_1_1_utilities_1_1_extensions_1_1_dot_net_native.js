var namespace_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native =
[
    [ "ArrayExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_array_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_array_extensions" ],
    [ "CharExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_char_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_char_extensions" ],
    [ "DoubleExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_double_extensions" ],
    [ "EnumExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_enum_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_enum_extensions" ],
    [ "ImageExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_image_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_image_extensions" ],
    [ "IntegerExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_integer_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_integer_extensions" ],
    [ "LinkedListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_linked_list_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_linked_list_extensions" ],
    [ "ListExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_list_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_list_extensions" ],
    [ "ObjectExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_object_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_object_extensions" ],
    [ "StringExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_string_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_string_extensions" ],
    [ "TypeExtensions", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_type_extensions.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_type_extensions" ],
    [ "XFile", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_x_file.html", "class_structures_1_1_utilities_1_1_extensions_1_1_dot_net_native_1_1_x_file" ]
];