var class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings =
[
    [ "BuildMassMatrix", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html#ab5cfe5fb6115ea9a0221475239386e8b", null ],
    [ "NumberOfModes", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html#a51372b63a76105a92130118a82cd5b57", null ],
    [ "RunModalAnalysis", "class_structures_1_1_solver_design_models_1_1_settings_1_1_modal_analysis_settings.html#a72880308393ac492e9a33de3792fde28", null ]
];