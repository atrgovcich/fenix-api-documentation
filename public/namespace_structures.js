var namespace_structures =
[
    [ "Materials", "namespace_structures_1_1_materials.html", "namespace_structures_1_1_materials" ],
    [ "MathNetCustom", "namespace_structures_1_1_math_net_custom.html", "namespace_structures_1_1_math_net_custom" ],
    [ "Solver3D", "namespace_structures_1_1_solver3_d.html", "namespace_structures_1_1_solver3_d" ],
    [ "SolverDesignModels", "namespace_structures_1_1_solver_design_models.html", "namespace_structures_1_1_solver_design_models" ],
    [ "Utilities", "namespace_structures_1_1_utilities.html", "namespace_structures_1_1_utilities" ]
];