var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel =
[
    [ "AssemblyDescription", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#a300ef3516916834de6b7859d424ab0a7", null ],
    [ "MaxAspectRatio", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#a62600502cdde4d7484e6be63bd84f0af", null ],
    [ "ScrewEdgeSpacing", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#a1c22de9840b8027c6aa6f73c727322bf", null ],
    [ "ScrewFieldSpacing", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#abd687defa09dd942065d9254c6301741", null ],
    [ "ScrewSizeOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#a0bca78f83831d7c4abeb4fed6666426e", null ],
    [ "StudAndTrackThicknessOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html#a327ae72e1d6d43569b8c756b29119f2f", null ]
];