var namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra =
[
    [ "ColdFormedResources", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_cold_formed_resources.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_cold_formed_resources" ],
    [ "DesignObject", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_object.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_object" ],
    [ "DesignResults", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_design_results" ],
    [ "DiaphragmPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel" ],
    [ "MechanicalDCR", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mechanical_d_c_r.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mechanical_d_c_r" ],
    [ "MemberDemands", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_member_demands.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_member_demands" ],
    [ "Mils", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_mils" ],
    [ "Screw", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_screw.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_screw" ],
    [ "ShearWallPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_shear_wall_panel" ],
    [ "SheathingMaterial", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_material.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_material" ],
    [ "SheathingPanel", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_panel.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_panel" ],
    [ "SheathingThickness", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_thickness.html", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_sheathing_thickness" ],
    [ "StudShape", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape.html", "struct_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_stud_shape" ],
    [ "BucklingAxis", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1", [
      [ "X", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1a02129bb861061d1a052c592e2dc6b383", null ],
      [ "Y", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1a57cec4137b614c87cb4e24a3d003a3e0", null ],
      [ "Torsion", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1a326dff39a0698a0740c8be77528b89b8", null ],
      [ "w", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1af1290186a5d0b1ceab27f4e77c0c5d68", null ],
      [ "z", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a3164dd39553f70ee9f8f6a27eb7e74b1afbade9e36a3f36d3d676c1b808451dd7", null ]
    ] ],
    [ "DesignApproachType", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a8b09bf9b914d0c0975244aa3df96fd6e", [
      [ "CapacityBasedDesign", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a8b09bf9b914d0c0975244aa3df96fd6ea7aa5ea92d53cdddd32c3d50a5f983664", null ],
      [ "CostBasedDesign", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a8b09bf9b914d0c0975244aa3df96fd6eacc6f732688159a8e92a127fcd19cf5cd", null ]
    ] ],
    [ "ExposureLocation", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a26b5612e33fca37c4d9cb37e67cc19a8", [
      [ "Interior", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a26b5612e33fca37c4d9cb37e67cc19a8aebbcc9b3b8fc5c7d79d0126910b82a69", null ],
      [ "Exterior", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a26b5612e33fca37c4d9cb37e67cc19a8ad9b3dceaf520ba1fa45c371c498c6baf", null ]
    ] ],
    [ "MechanicalAction", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254e", [
      [ "Bending", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea5b5216636841252051e688193e81f767", null ],
      [ "Shear", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea02414922b70cc0f9d7c841b0c70a0f94", null ],
      [ "CompressionPerpendicular", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea0d9606e93128397518e31c134c7dee89", null ],
      [ "CompressionParallel", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea44daf41f64184f768e912254a094d58d", null ],
      [ "Tension", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea2b110ae140c35594fdfa8a5bfe402f8b", null ],
      [ "CombinedCompressionParallelPlusBending", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea289cbb63f12e0d47e3d35690b3186867", null ],
      [ "CombinedTensionPlusBending", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ead11affc2f5784c9070ae18c81e8effbd", null ],
      [ "None", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#ac62230f289e5275ba1f31fe10eca254ea6adf97f83acf6453d4a6a4b1070f3754", null ]
    ] ],
    [ "ShapeCountryOfOrigin", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#af0f19f7d18c97b2371777f47f63c202c", [
      [ "UnitedStatesOfAmerica", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#af0f19f7d18c97b2371777f47f63c202ca10b33411b9aa99b644b6cc4cfcb6213a", null ],
      [ "Australia", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#af0f19f7d18c97b2371777f47f63c202ca4442e4af0916f53a07fb8ca9a49b98ed", null ]
    ] ],
    [ "SheathingType", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467", [
      [ "OSB", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467adc5a1f36bb02b05d0df0e790d451f043", null ],
      [ "Plywood", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467af2e20fa00c5e380e0a62435a943e466e", null ],
      [ "Gypsum", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467a6b8928ebc900f4e5526ac7897cfdc25f", null ],
      [ "Fiberboard", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467ae5199feb6bf248953c715031f6dca487", null ],
      [ "CSP", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467acbc6e9792f97c17c170caeb83895c44e", null ],
      [ "SheetSteel", "namespace_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra.html#a375398280cf9f6b622893a9ebfdc4467a0c7fe243ce1dc39502c735d311b6f4b3", null ]
    ] ]
];