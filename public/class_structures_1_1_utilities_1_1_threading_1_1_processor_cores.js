var class_structures_1_1_utilities_1_1_threading_1_1_processor_cores =
[
    [ "IHardwareCore", "interface_structures_1_1_utilities_1_1_threading_1_1_processor_cores_1_1_i_hardware_core.html", "interface_structures_1_1_utilities_1_1_threading_1_1_processor_cores_1_1_i_hardware_core" ],
    [ "CurrentLogicalCore", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores.html#a9ce49c100c2225a128c3a9930f9cfc44", null ],
    [ "HardwareCores", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores.html#ae5a1568c875e4ec104411a630a353f2e", null ],
    [ "LogicalCores", "class_structures_1_1_utilities_1_1_threading_1_1_processor_cores.html#add8d5fe4e82b649d6b6533f07dd7767e", null ]
];