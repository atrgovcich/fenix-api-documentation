var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group =
[
    [ "ElementGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a767ed947f88a9e7f52a81c30ededa3a9", null ],
    [ "ElementGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a78cdd3bfbe2bc9fdf853a57d09c96b99", null ],
    [ "AddGroupType", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#aa47658672ec1932e56df2be15fe23111", null ],
    [ "RemoveGroupType", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a47781a6d82adf101c3f3d18490e89f20", null ],
    [ "ColorHashName", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#ae2a28e8bab6207fa25976efdffcca8a1", null ],
    [ "Diaphragm", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a6d02c686e449b749e9909c9bf6f61f75", null ],
    [ "DisplayColor", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a8431262909c0b0481ea7dc1736a0ada4", null ],
    [ "GroupTypes", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#af93abadf4f160ed6d7854ba9c51d2e6f", null ],
    [ "ID", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a47e4e570d620c2f19da1a73c11910a8a", null ],
    [ "IsDiaphragm", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a7141b79e310999384912ef957d1c0b8b", null ],
    [ "Name", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#af36001da84b8002d947db7e9a1534965", null ],
    [ "Opacity", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_element_group.html#a4ced0dad1d406d03971caa12fd3342f0", null ]
];