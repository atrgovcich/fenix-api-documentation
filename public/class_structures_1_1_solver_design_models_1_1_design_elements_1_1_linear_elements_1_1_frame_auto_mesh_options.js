var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options =
[
    [ "FrameAutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a0474ac7f7319a876f01aad1af0fd9c36", null ],
    [ "FrameAutoMeshOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#ad1fb1f7232113d5dbbb22ffa8a1b2138", null ],
    [ "Copy", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#ac4a23b68095389272c08c24b9cf82e99", null ],
    [ "IncludeInAreaMesh", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a1545a0d77f10ebcafd28f4f8c1824e6f", null ],
    [ "MeshAtIntersectingFrames", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a53155ea46bb77302f61c7b762f914637", null ],
    [ "MeshAtIntersectingLinks", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a9e09039ed8b903f4fbb9077ffb698f82", null ],
    [ "MeshAtIntersectingNodes", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a893c690c21672c2459c9bd1d5d4eccc0", null ],
    [ "MeshAtIntersectingShells", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a549e42bf2f8ba7f25b6a99569f4b0f69", null ],
    [ "MeshAtIntersectingSolids", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a657435826e7840767a8eb980ed4c2470", null ],
    [ "MinimumSubdivisions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_frame_auto_mesh_options.html#a9e1a280f0305dfa819497157a4a83f5c", null ]
];