var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options =
[
    [ "BracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#a31e6685c2995a7e959f8b1bf9589f781", null ],
    [ "BracingOptions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#a4cd28ae7717d9bec633fdd7b532b4e51", null ],
    [ "BracedAtIntersectingFrames", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#a84d3d81cb0dae5ad6b5ba169841b3cd3", null ],
    [ "BracedAtIntersectingShells", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#ad825c91e76c316d9c5e0ac8a9dbcc574", null ],
    [ "BracedAtInterval", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#a82b05f3ff058d57c164f4cec684a7231", null ],
    [ "BracingInterval", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#ad2ce1e476daa6f91b9e2116ae8aec1e9", null ],
    [ "ContinuouslyBraced", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#acb8c5450f1b93478ec0af7fff21f62f1", null ],
    [ "MaximumFrameAngleToConsiderBraced", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#af70cd18a0c78b82e64c7dc320409875a", null ],
    [ "MinimumFrameAngleToConsiderBraced", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#af5b294686a7292d63da544399114b561", null ],
    [ "OverrideFrameLength", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_linear_elements_1_1_bracing_options.html#aed15b60675703569e1507502863a310f", null ]
];