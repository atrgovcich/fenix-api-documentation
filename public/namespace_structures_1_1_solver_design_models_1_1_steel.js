var namespace_structures_1_1_solver_design_models_1_1_steel =
[
    [ "SteelResources", "class_structures_1_1_solver_design_models_1_1_steel_1_1_steel_resources.html", "class_structures_1_1_solver_design_models_1_1_steel_1_1_steel_resources" ],
    [ "WidthToThickness", "class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness.html", "class_structures_1_1_solver_design_models_1_1_steel_1_1_width_to_thickness" ],
    [ "SteelShapeCategory", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008", [
      [ "WideFlange", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008aab0bac0aa8fe80238859c34c3462843c", null ],
      [ "RectangularHSS", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a1d9b649b1e06fb917e1c3e54ef7ce5bc", null ],
      [ "RoundHSS", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a84b63e377a186ef4b6375c8f45b33209", null ],
      [ "SingleAngle", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a4541038af095e50a93a8edf53bc2c6e4", null ],
      [ "DoubleAngle", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a6daea3df23b4391c9947acf987a04f60", null ],
      [ "Channel", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a781dc97dc62331eec3ea9ec4373a3ca8", null ],
      [ "MiscChannel", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a8be63a33ae9be8a035de8d8bf4d4fb39", null ],
      [ "WideFlangeTee", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a5eb9c2c90b565109fded6e4e5e76e42f", null ],
      [ "BuiltUp", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a06a9d6a37731cfc5dcf821e18cd14090", null ],
      [ "ParallelFlangeChannel", "namespace_structures_1_1_solver_design_models_1_1_steel.html#af140cd34a4a7eebae8c58628258b6008a9b4a52cf1e91df59d083a66a9ef85cbe", null ]
    ] ]
];