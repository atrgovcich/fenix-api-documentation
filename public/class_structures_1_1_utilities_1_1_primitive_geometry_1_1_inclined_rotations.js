var class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations =
[
    [ "InclinedRotations", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#ae7dda35e1522da5b2417735c1c8cf963", null ],
    [ "XInDegrees", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#ac3dff4a9f7e103247534b478aaf2d3dc", null ],
    [ "XInRadians", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#a052f5afd33422db92806263a2855323a", null ],
    [ "YInDegrees", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#a459f311cfa3be18d8506f5a72083dc3d", null ],
    [ "YInRadians", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#ac334972b68709cc6f735132e01cfed25", null ],
    [ "AboutXAxis", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#a5646278aed4b2be1dfa6c932365864d5", null ],
    [ "AboutYAxis", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_inclined_rotations.html#a5eb24eae131ab3d8b60ecb2a902197dd", null ]
];