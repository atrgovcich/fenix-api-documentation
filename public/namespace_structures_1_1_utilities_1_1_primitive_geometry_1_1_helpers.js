var namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers =
[
    [ "SimplePolygonException", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers_1_1_simple_polygon_exception.html", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers_1_1_simple_polygon_exception" ],
    [ "Offset2DSense", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers.html#addea3e24931462fb26eeeb825db4ee2d", [
      [ "Left", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers.html#addea3e24931462fb26eeeb825db4ee2da945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Right", "namespace_structures_1_1_utilities_1_1_primitive_geometry_1_1_helpers.html#addea3e24931462fb26eeeb825db4ee2da92b09c7c48c520c3c55e497875da437c", null ]
    ] ]
];