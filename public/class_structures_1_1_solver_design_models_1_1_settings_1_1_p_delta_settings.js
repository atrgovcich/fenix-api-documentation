var class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings =
[
    [ "PDeltaSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html#a75d1f402fc3247a6254f4c2e9d9334dd", null ],
    [ "PDeltaSettings", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html#a6a42fac3aef281da36c2b8d9b4b906c5", null ],
    [ "IncludePDelta", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html#a6ebc85da93b866c749ef516eff3e4c69", null ],
    [ "MaxNumberOfPDeltaIterations", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html#a5523903ca2f9a038ddda02894192255f", null ],
    [ "PDeltaConvergenceThreshold", "class_structures_1_1_solver_design_models_1_1_settings_1_1_p_delta_settings.html#a70ad03537765fd1ce7d1fb774fb100d2", null ]
];