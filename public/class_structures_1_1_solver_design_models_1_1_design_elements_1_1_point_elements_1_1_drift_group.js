var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group =
[
    [ "DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#af2a350d9513fb239b2c3e513918bc56c", null ],
    [ "DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a57b9004fb013d2423a363da5eefad196", null ],
    [ "DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a40ba7059906aac2aac598461ac64e10c", null ],
    [ "DriftGroup", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a0c6998c6d791db94427ecdc2fbcc64e4", null ],
    [ "AddDriftStack", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a9028ab77cee1f055eec073b5d225fd1c", null ],
    [ "AddNodalDrift", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a851818fb772934c51cb846f12a464115", null ],
    [ "ClearDriftResults", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a2628575f254701d8c74c20c9a9b664cc", null ],
    [ "GetDriftStackAtCombo", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#af5bbccb706e4ea3234547710049ce279", null ],
    [ "GetDriftStackAtCombo", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#ada071db9e9b9da6e27d2452eb1c32c41", null ],
    [ "GetNodeList", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a12be0a13deb89cde8ef639246997954d", null ],
    [ "SetNodeList", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#afa11ea86af2c579e8318e04fa968f82a", null ],
    [ "VerticalAxisVector", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a8cdd5dbd842ec288775f19b4dec1379a", null ],
    [ "GUID", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#ad8038b20cda3db42f1bc02ef45dc52bb", null ],
    [ "ID", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a3c94b87ed5277c075c83f7441e22b754", null ],
    [ "IsAutoDrift", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a115212448d50588982be07fd5dc6d6b0", null ],
    [ "Name", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a2e877c9bacd80f516fe4bfc66226a174", null ],
    [ "VerticalAxis", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_point_elements_1_1_drift_group.html#a559b8d559f77bd4b34c93a2311d16afb", null ]
];