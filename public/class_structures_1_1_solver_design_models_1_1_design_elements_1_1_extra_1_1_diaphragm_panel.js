var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel =
[
    [ "IsBlocked", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html#a37b6c18ef53cc836c49e3539c87c444b", null ],
    [ "ScrewBoundaryEdgeSpacing", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html#a159753016a2da971c5c7127a0bb866d7", null ],
    [ "ScrewFieldSpacing", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html#a715d655d145005e60c5846852159c7aa", null ],
    [ "ScrewOtherEdgeSpacing", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_extra_1_1_diaphragm_panel.html#ad22ed3afa849dd5409e571ae7c758eae", null ]
];