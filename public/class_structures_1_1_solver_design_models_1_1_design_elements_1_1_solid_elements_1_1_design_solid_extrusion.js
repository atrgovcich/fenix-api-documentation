var class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion =
[
    [ "ClearAnalysis", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#afc631979a8b0bae1381323fd57d89d69", null ],
    [ "Faces", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a4fcfb94f1b3480478201730c81ec58eb", null ],
    [ "GetMinMaxBounds", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a72bf8dbf950d5a8a1527fcc6a8796a19", null ],
    [ "MeshElement", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#add63288ff809705657f4893cbbc24749", null ],
    [ "OctreeBoundingBox", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a8e21ffb4fcd40370c516fd52b0c458cd", null ],
    [ "AnalysisElements", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a17247ea20d73566505b689ce77a218bb", null ],
    [ "AreaToExtrude", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#ad1a6ba94163a0289615a08ca28cceec6", null ],
    [ "Edges", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#ab4b5f7e6023736f5cf77da67fbc0f0ce", null ],
    [ "ElementType", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a0fff0b723636b1e09cb753e1d110ba29", null ],
    [ "ExtrusionDirection", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a796eb73051f4911ee9b666fa55b29bdd", null ],
    [ "ExtrusionDistance", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#ab476f6c997ef74a8dc0ebaae01def430", null ],
    [ "NumberOfExtrusions", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a7a3c4bc42308865a0ea1036c2aac6400", null ],
    [ "SolidElementType", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#a723ec66e0fbaf28c3ea4767b0660214d", null ],
    [ "Vertices", "class_structures_1_1_solver_design_models_1_1_design_elements_1_1_solid_elements_1_1_design_solid_extrusion.html#aa07e32a1fe36e1e80a0211b1fcddf389", null ]
];