var class_structures_1_1_utilities_1_1_http_requests_1_1_metadata =
[
    [ "modelVersion", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a6d39c89a4a5692a8330c26a113454dda", null ],
    [ "pgadFloor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a33ee889f7612a7a00836d08808b7ed9d", null ],
    [ "pgadPercentileFactor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#ad5847a6afcb57735695968f64697d00f", null ],
    [ "s1dFloor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#aadf302270408b59674c70bd2236b4873", null ],
    [ "s1dPercentileFactor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a9f06ce0c2867de0e281f82e556372c12", null ],
    [ "s1MaxDirFactor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a50adcee0279178f8f15f5865a8645894", null ],
    [ "spatialInterpolationMethod", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a0bf99849202c33d8c270aff722ada029", null ],
    [ "ssdFloor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a49d91802aaca7ef52a2ab087b8e704db", null ],
    [ "ssdPercentileFactor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a30d67ea4385019dcae1adc6cf3f5c8f3", null ],
    [ "ssMaxDirFactor", "class_structures_1_1_utilities_1_1_http_requests_1_1_metadata.html#a50858b3e9e2c0b014be2bd524b583116", null ]
];