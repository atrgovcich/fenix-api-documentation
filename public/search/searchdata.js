var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "abcdefghiklmnopqrstuvwx",
  2: "s",
  3: "abcdefghijklmnopqrstuvwxyz",
  4: "_abcdefghijklmnopqrstuvxyz",
  5: "abcdefghilmnorstuw",
  6: "abcdefghiklmnopqrstuvwxyz",
  7: "abcdefghijklmnopqrstuvwxyz",
  8: "cipv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events"
};

