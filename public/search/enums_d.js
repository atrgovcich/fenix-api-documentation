var searchData=
[
  ['resultlimittype_0',['ResultLimitType',['../namespace_structures_1_1_utilities_1_1_reporting_1_1_equations.html#a16a3b9b2a37e3f715ae6704f2a57e1a0',1,'Structures::Utilities::Reporting::Equations']]],
  ['resulttype_1',['ResultType',['../namespace_structures_1_1_utilities_1_1_loads.html#ab479d4d91610f29db26171c51a7c6ef6',1,'Structures::Utilities::Loads']]],
  ['riskcategory_2',['RiskCategory',['../namespace_structures_1_1_utilities_1_1_loads.html#a03257f8c2b410ec88ca7164fca4b8b40',1,'Structures::Utilities::Loads']]],
  ['roofexposurecategory_3',['RoofExposureCategory',['../namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a6ff237e235d4cca519abfa6247ed2d85',1,'Structures::Utilities::Loads::Snow::ASCE_7_16']]],
  ['roofsurface_4',['RoofSurface',['../namespace_structures_1_1_utilities_1_1_loads_1_1_snow_1_1_a_s_c_e__7__16.html#a5108aa974a4c356f01fea015c0891aaa',1,'Structures::Utilities::Loads::Snow::ASCE_7_16']]]
];
