var class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions =
[
    [ "Centroid", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a4345bfaa74c3d695c9ef7d1b4f4bcfb7", null ],
    [ "HasCoincidentEndPointAs", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a6e95fe8e22cd946911835fc7cf6a229b", null ],
    [ "HorizontalProjectionIsCollinearWith", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a6f3a72be09561abd670280c388221ccb", null ],
    [ "IntersectsWith", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#abc5d09a2bca0f31033d0f25a6c90008c", null ],
    [ "IntersectsWith2", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a0251908e79cc1576d53659f0564ac005", null ],
    [ "IntersectsWith3", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a3c31a9b8462fdfcab969978c38123870", null ],
    [ "IntersectsWith4", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a92b127dac6033569d6caa9a14e7ae006", null ],
    [ "IntersectsWith5", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#afc095b7c84792f3d6ab21fa993c5266b", null ],
    [ "IntersectsWithPolygon", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#ae1f91489029f747fb211a10597c81909", null ],
    [ "IntersectsWithPolygonEdges", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#aa2aac56a2bcca472c1d5d3b13a07a032", null ],
    [ "IntersectsWithPolygonInSamePlane", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a79775a5ebad0500dfc3674e4881838b5", null ],
    [ "IntersectsWithPolygonPlane", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#ac98afa7f3653f89a168ff56408ad5529", null ],
    [ "IsCoincidentWith", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a5018875c8fdf737f9994b161a43f97b5", null ],
    [ "LiesInPolygonPlane", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a363d6f14bfa04454c6ad9e1c9939a1e0", null ],
    [ "LiesOnPolygon", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#af4c94a58f6bade9d9ead5e3c1962314c", null ],
    [ "Midpoint", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a69646ba440d4e78fa6dda07520fba4c6", null ],
    [ "OctreeBoundingBox", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a2c65fb5ef010afadda7174d81f100bce", null ],
    [ "ToSegment2D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#af847680172f6195b37135d38e7d3c3f1", null ],
    [ "ToUnitVector3D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a7c2adc790e9e6a44cab3496c055a7c28", null ],
    [ "ToVector3D", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a345661b5ab59a108859ace8c48a367a4", null ],
    [ "UnitVector", "class_structures_1_1_utilities_1_1_primitive_geometry_1_1_extensions_1_1_line_segment3_d_extensions.html#a85e74a4e0a4d0ce142de4581c4229842", null ]
];